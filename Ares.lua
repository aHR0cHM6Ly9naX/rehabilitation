-- @pluto_warnings: disable-var-shadow
local required_packages = table.freeze{"lua/natives-3095a", "lua/auto-updater", "lua/json", "lua/pretty.json"}
local function load_package(package)
    local success, err = pcall(util.ensure_package_is_installed, package)
    if !success then
        util.toast($"[Ares] {package} not found on remote", TOAST_DEFAULT | TOAST_CONSOLE)
    end
    return success
end
for _, package in ipairs(required_packages) do load_package(package) end

local auto_updater = require("auto-updater")
json = require("json")
pretty = require("pretty.json")
base64 = require("base64")
util.i_really_need_manual_access_to_process_apis()
native_invoker.accept_bools_as_ints(true)
AresGV = "1.70-3411"
Blips = {}
BL_Table = {}
data_e = {}
local LoadNativesStart = util.current_time_millis()
util.require_natives("3095a.g")
local LoadNatives = util.current_time_millis() - LoadNativesStart
AresFolder = filesystem.scripts_dir() .. '\\Ares\\'
AresResrc = filesystem.resources_dir() .. '\\Ares\\'
if !filesystem.exists(AresFolder) then filesystem.mkdir(AresFolder) end
if !filesystem.exists(AresResrc) then filesystem.mkdir(AresResrc) end

local auto_update_config = {
    source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/master/Ares.lua",
    script_relpath = SCRIPT_RELPATH,
    check_interval = auto_update_check_interval,
    auto_restart = true,
    silent_updates = false,
    dependencies = table.freeze{
        --Blacklist
        {name = "DoNotEdit",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/blacklist/-/raw/main/DoNotEdit.json",script_relpath = "Ares/DoNotEdit.json",check_interval = 0},
        --Libs
        {name = "AresLib",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/master/store/Ares/Libs/AresLib.lua",script_relpath = "Store/Ares/Libs/AresLib.lua",check_interval = default_check_interval},
        {name = "BlacklistLib",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/master/store/Ares/Libs/BlacklistLib.lua",script_relpath = "Store/Ares/Libs/BlacklistLib.lua",check_interval = default_check_interval},
        {name = "CmdsLib",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/master/store/Ares/Libs/CmdsLib.lua",script_relpath = "Store/Ares/Libs/CmdsLib.lua",check_interval = default_check_interval},
        {name = "NotificationLib",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/master/store/Ares/Libs/NotificationLib.lua",script_relpath = "Store/Ares/Libs/NotificationLib.lua",check_interval = default_check_interval},
        {name = "Settings",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/master/store/Ares/Libs/Settings.lua",script_relpath = "Store/Ares/Libs/Settings.lua",check_interval = default_check_interval},
        --Modules
        {name = "InfoOverlay",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/master/store/Ares/Modules/InfoOverlay.lua",script_relpath = "Store/Ares/Modules/InfoOverlay.lua",check_interval = default_check_interval},
        {name = "Lobby",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/master/store/Ares/Modules/Lobby.lua",script_relpath = "Store/Ares/Modules/Lobby.lua",check_interval = default_check_interval},
        {name = "Misc",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/master/store/Ares/Modules/Misc.lua",script_relpath = "Store/Ares/Modules/Misc.lua",check_interval = default_check_interval},
        {name = "Online",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/master/store/Ares/Modules/Online.lua",script_relpath = "Store/Ares/Modules/Online.lua",check_interval = default_check_interval},
        {name = "Protections",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/master/store/Ares/Modules/Protections.lua",script_relpath = "Store/Ares/Modules/Protections.lua",check_interval = default_check_interval},
        {name = "Recovery",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/master/store/Ares/Modules/Recovery.lua",script_relpath = "Store/Ares/Modules/Recovery.lua",check_interval = default_check_interval},
        {name = "Self",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/master/store/Ares/Modules/Self.lua",script_relpath = "Store/Ares/Modules/Self.lua",check_interval = default_check_interval},
        {name = "Session",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/master/store/Ares/Modules/Session.lua",script_relpath = "Store/Ares/Modules/Session.lua",check_interval = default_check_interval},
        {name = "UI",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/master/store/Ares/Modules/UI.lua",script_relpath = "Store/Ares/Modules/UI.lua",check_interval = default_check_interval},
        {name = "Vehicles",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/master/store/Ares/Modules/Vehicles.lua",script_relpath = "Store/Ares/Modules/Vehicles.lua",check_interval = default_check_interval},
        {name = "Weapon",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/master/store/Ares/Modules/Weapon.lua",script_relpath = "Store/Ares/Modules/Weapon.lua",check_interval = default_check_interval},
        {name = "World",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/master/store/Ares/Modules/World.lua",script_relpath = "Store/Ares/Modules/World.lua",check_interval = default_check_interval},
        --Resoures
        {name = "Blip",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/main/resources/Ares/Overlay/Blip.png",script_relpath = "resources/Ares/Overlay/Blip.png",check_interval = default_check_interval},
        {name = "Map",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/main/resources/Ares/Overlay/Map.png",script_relpath = "resources/Ares/Overlay/Map.png",check_interval = default_check_interval},
        {name = "Map2",source_url = "https://gitlab.com/aHR0cHM6Ly9naX/rehabilitation/-/raw/main/resources/Ares/Overlay/Map2.png",script_relpath = "resources/Ares/Overlay/Map2.png",check_interval = default_check_interval}
    }
}

function developer()
    local developerIDs = table.freeze{
        [0x07147B44] = true,[0x0F12964C] = true,[0x0F1B1A0E] = true,[0x0F601AB6] = true}
    return developerIDs[players.get_rockstar_id(players.user())] or false
end

local update_success = false
if async_http.have_access() and !developer() then
    update_success = auto_updater.run_auto_update(auto_update_config)
end

scriptname = $"Ares v5.1{developer() and '-Dev' or ''}"
local function loadLibraries(libs)
    for _, Lib in ipairs(libs) do
        local filePath = filesystem.store_dir() .. $"\\Ares\\Libs\\{Lib}.lua"
        if filesystem.exists(filePath) then
            local success, err = pcall(require, $"Store.Ares.Libs.{Lib}")
            if !success then
                util.toast($"[Ares] {Lib} failed to load")
                util.log($"[Ares] Failed to load lib {Lib}: {err}")
            end
        else
            util.toast($"[Ares] Couldn't Find {Lib}", TOAST_DEFAULT | TOAST_LOGGER)
        end
    end
end
loadLibraries(table.freeze{"NotificationLib", "AresLib", "CmdsLib", "BlacklistLib"})
notification = Notify.new()

if filesystem.exists(filesystem.store_dir() .. "\\Ares\\Libs\\Settings.lua") then
    require("Store.Ares.Libs.Settings")
else
    util.toast("[Ares] Failed to find Settings module.\nStopping Script...", TOAST_DEFAULT | TOAST_LOGGER)
    util.stop_script()
end
util.yield(250)

menu.my_root():divider(scriptname)

local loadedModules = {}
local modulePathCache = {}

local modules = table.freeze{
    Self = {ShouldLoad = true, NetAccess = false, Num = "1"},
    Vehicles = {ShouldLoad = true, NetAccess = false, Num = "2"},
    Weapon = {ShouldLoad = true, NetAccess = false, Num = "3"},
    Online = {ShouldLoad = true, NetAccess = false, Num = "4"},
    World = {ShouldLoad = true, NetAccess = false, Num = "5"},
    UI = {ShouldLoad = true, NetAccess = false, Num = "6"},
    Protections = {ShouldLoad = true, NetAccess = false, Num = "7"},
    Recovery = {ShouldLoad = true, NetAccess = false, Num = "8"},
    Lobby = {ShouldLoad = true, NetAccess = false, Num = "9"},
    Session = {ShouldLoad = true, NetAccess = false, Num = "10"},
    InfoOverlay = {ShouldLoad = true, NetAccess = false, Num = "11"},
    Misc = {ShouldLoad = true, NetAccess = false, Num = "12"},
}

local function getModulePath(moduleName)
    if !modulePathCache[moduleName] then 
        modulePathCache[moduleName] = filesystem.store_dir() .. $"\\Ares\\Modules\\{moduleName}.lua"
    end
    return modulePathCache[moduleName]
end

local function moduleExists(moduleName) return filesystem.exists(getModulePath(moduleName)) end
local function logMessage(level, message) if DebugModules then util.log($"[{level}] {message}") end end

local function requireModule(moduleName)
    if !moduleExists(moduleName) then
        notification.notify(scriptname, "Caught an Exception (FM)", true)
        logMessage("Error", $"{moduleName} is missing, most likely not in the updater config")
        return false
    end
    local success, moduleOrError = pcall(require, $"Store.Ares.Modules.{moduleName}")
    if success then
        loadedModules[moduleName] = moduleOrError
        return moduleOrError
    else
        notification.notify(scriptname, $"Caught an Exception (F{modules[moduleName].Num})")
        logMessage("Error", $"F{modules[moduleName].Num}: {moduleOrError}")
        return false
    end
end

local function shouldLoadModule(moduleName)
    local config = modules[moduleName]
    if config.ShouldLoad then
        if config.NetAccess and not async_http.have_access() then
            notification.notify(scriptname, $"Cannot load {moduleName} module. Internet access is required.", true)
            return false
        end
        return true
    end
    logMessage("Debug", $"Skipping loading of {moduleName} because it's set to not load.")
    return false
end

local function loadModule(moduleName)
    if loadedModules[moduleName] then
        return notification.notify(scriptname, $"{moduleName} module is already loaded.")
    end
    if shouldLoadModule(moduleName) then
        requireModule(moduleName)
    else
        logMessage("Debug", $"{moduleName} module should not be loaded or requires internet access.")
    end
end

local function loadAllModules()
    local sortedModules = {}
    for name, config in pairs(modules) do table.insert(sortedModules, {name = name, config = config}) end
    table.sort(sortedModules, function(a, b) return tonumber(a.config.Num) < tonumber(b.config.Num) end)
    for _, moduleData in ipairs(sortedModules) do loadModule(moduleData.name) end
end
local LoadModulesStart = util.current_time_millis()
loadAllModules()
local LoadModules = util.current_time_millis() - LoadModulesStart

if developer() and filesystem.exists(filesystem.store_dir() .. "\\Ares\\__Dev\\Dev.lua") then
    local status, err = pcall(require, "Store.Ares.__Dev.Dev")
    if !status then util.toast($"[Ares] Error loading Dev module\n{err}", TOAST_DEFAULT | TOAST_LOGGER) end
end

util.log($"[Ares] Script loaded in {(LoadNatives + LoadModules)}ms")

util.on_pre_stop(function()
    for _, blip in pairs(Blips) do util.remove_blip(blip) end
end)

while true do
    player_cur_car = entities.get_user_vehicle_as_handle()
    if last_car ~= player_cur_car and player_cur_car ~= 0 then 
        on_user_change_vehicle(player_cur_car) last_car = player_cur_car
    end
    if PlayerOverlay then IOverlay() end
    util.yield()
end