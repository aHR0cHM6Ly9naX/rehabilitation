# Ares

A script to enhance gameplay.

## Notes

The blacklist will update every time you launch the Lua script. You can check the official [Blacklist](https://gitlab.com/aHR0cHM6Ly9naX/blacklist).

## Credits

This script is not public, so there are no credits included in the code.

However, special thanks to:
- Prisuhm
- Lena
- Wiri
- MrRobot