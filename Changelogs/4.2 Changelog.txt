4.2 Changelog >

[Notes]

Now archiving the old changelogs

Removed features have a tag for why they were removed

Auto updater now works unless you're marked as a dev within the script.

[New]

Self Options > Auto Restock

World Options > Gun Van Options

Online Options > Mission Helper
Online Options > Banking Options

Recovery Options > Stat Options

Recovery Options > Unlock Options > Achievement Unlocker

Recovery Options > Money Options > Settings > Disable Transaction Errors
Recovery Options > Money Options > Casino Scam > Roulette

[Work In Progress]

[Changes]

An "is working" check for globals / script events.

Moved attachment options into it's own submenu
Extra Cleanup in AresLib
Recoded Holster Weapon

Slowly changing Ares into a modular script

Changed it so some options don't show unless you got the correct stand version

[Removed]

Work Out This Equation [Merged into Tie Them Up]
Preset Message Kick
Taxi Scam
Achievement Unlocker