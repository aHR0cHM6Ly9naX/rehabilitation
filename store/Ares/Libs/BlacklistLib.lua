local user_export_blacklist_path = AresFolder .. 'SharedBlacklists/'..players.get_name(players.user())..'_Blacklist.json'
local function ReadFile(the_path)
    if type(the_path) ~= "string" then
        --print("[Ares] ERROR: the_path must be a string.")
        return
    end    
    local file = io.open(the_path, "r")
    if file then
        local content = file:read("*all")
        file:close()
        return content
    else
    --    print("[Ares] ERROR: Could not read file")
        return nil
    end
end

local function WriteFile(the_path, payload)
    if type(the_path) ~= "string" then
        --print("[Ares] ERROR: the_path must be a string.")
        return
    end    
    if not filesystem.exists(the_path) then
        local directory = the_path:match("(.*/)")
        if directory then
            filesystem.mkdir(directory)
        end
    end
    local file = io.open(the_path, "w")
    if file then
        file:write(payload)
        file:close()
    --else
    --    print("[Ares] ERROR: Could not open file for writing.")
    end
end

if !filesystem.exists(user_export_blacklist_path) then
    local old_export = ReadFile(AresFolder .. 'SharedBlacklists/ExportBlacklist.json')
    if old_export then
        WriteFile(user_export_blacklist_path, old_export)
        os.remove(AresFolder .. 'SharedBlacklists/ExportBlacklist.json')
    else
        WriteFile(user_export_blacklist_path, "[]")
    end
end

function SaveDataE()
    local data_e_status, data_encoded_e = pcall(json.encode, data_e)
    if data_e_status and data_encoded_e then
        WriteFile(user_export_blacklist_path, data_encoded_e)
    --else
    --    print("[Ares] ERROR: Failed to encode data_e.")
    end
end

local function InBlacklist(rid)
    local id = tostring(rid)
    return BL_Table[id] ~= nil
end

local function LoadDataE()
    for filesystem.list_files(AresFolder .. 'SharedBlacklists') as path do
        if string.contains(path, "_Blacklist.json") then
            local contents = ReadFile(path)
            if contents then
                -- Adds unneed prints to console, maybe add an error if it fails to load
                --util.log($"[Ares] Loaded Blacklist file: {string.match(path, "[^/\\]+$")}")
                if contents == '' then
                    contents = "{}"
                    WriteFile(user_export_blacklist_path, contents)
                end
                local old_data_e_status, old_data_e = pcall(json.decode, contents)
                if old_data_e_status and old_data_e then
                    for id, _ in pairs(old_data_e) do
                        BL_Table[id] = true
                    end
                end
            end
        end
    end
end

local function LoadDataG()
    local file = io.open(AresFolder .. 'DoNotEdit.json', 'r')
    if file then
        local contents = file:read('*all')
        io.close(file)
        local load_data_g_status, json_data_g = pcall(json.decode, contents)
        if load_data_g_status and json_data_g then
            for ipairs(json_data_g) as id do
                BL_Table[id] = true
            end
        end
    end
end

local function UpdateBlacklist()
    for players.list() as pid do
        local rid, name = players.get_rockstar_id(pid), players.get_name(pid)
        if InBlacklist(rid) then
            notification.notify(scriptname, $"{name} will be kicked due to being on the Blacklist.", true)
            RefWrapper($"Online>Player History>{players.get_name(pid)}>Player Join Reactions>Block Join")
            OnlineRefWrapper(menu.player_root(pid), "Kick>Love Letter")
            yield(30, "s")
        end
    end
end

players.on_join(UpdateBlacklist)
LoadDataE()
LoadDataG()