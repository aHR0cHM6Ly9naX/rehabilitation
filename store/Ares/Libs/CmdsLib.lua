local function restartFreemodeScript()
    if InSession() then
        RefWrapper("Online>Session>Restart Freemode Script")
    else
        notification.notify(scriptname, "You need to be online to use this!")
    end
end

local function restartStandConsole()
    local consoleRef = menu.ref_by_path("Stand>Console")
    if consoleRef and consoleRef:getState() then
        consoleRef:setState(false)
        yield(1, "s")
        consoleRef:trigger()
    end
end

local function SessionCode()
    local applicable, code = util.get_session_code()
    while applicable and (not code or code == "Please wait...") do
        util.yield(1000)
        applicable, code = util.get_session_code()
    end
    if applicable then return code else return "N/A" end
end

local hiddenCommands = table.freeze{
    {
        name = "Restart Freemode",
        aliases = {"rfm", "resetfm"},
        description = "Quicker commands for Stand's restart freemode",
        action = restartFreemodeScript
    },
    {
        name = "Restart Stand Console",
        aliases = {"rsc", "restartconsole"},
        description = "Restarts the stand console to clear it out",
        action = restartStandConsole
    },
    {
        name = "Restart Ares",
        aliases = {"rsa", "restartares", "rbeta"},
        description = "Restarts Ares & all required files",
        action = function() util.restart_script() end
    },
    {
        name = "Restart GTA",
        aliases = {"rgta", "restartgta"},
        description = "Restarts GTA",
        action = RESTART_GAME
    },
    {
        name = "Close GTA",
        aliases = {"tk"},
        description = "Instantly closes GTA using taskkill",
        action = function() os.execute("taskkill /F /IM GTA5.exe") end
    },
    {
        name = "Session Code",
        aliases = {"sessioncode", "getcode"},
        description = "Copes the session code into the command format",
        action = function()
            local code = SessionCode()
            if code ~= "N/A" and code ~= "Please wait..." then
                util.copy_to_clipboard($"[{SessionType()} Session.🡭](https://stand.gg/join#{code})\nOr Copy Command: `codejoin {code}`", false)
            else
                notification.notify(scriptname, $"This session dosnt have a invite code right now.")
            end
        end
    },
    {
        name = "Quick Bail",
        aliases = {"qb", "quickb", "bail"},
        description = "Shortcut for Quick Bail",
        action = function() RefWrapper("Stand>Experiments>Quick Bail") end
    },
}

local function registerCommands()
    local cmds = menu.my_root():list("Hidden Commands", {}, "This shouldn't be showing!")
    menu.set_visible(cmds, false)
    local printcmds = cmds:action("Print Command List", {"pcmds"}, "Prints all command only features into console", function()
        local outputLines = {}
        for _, cmd in ipairs(hiddenCommands) do
            local line = cmd.name .. " | " .. table.concat(cmd.aliases, ", ")
            table.insert(outputLines, line)
        end
        local finalOutput = table.concat(outputLines, "\n > ")
        util.log($"[Ares] Command List-\n > {finalOutput}")
    end)
    menu.set_visible(printcmds, false)
    for _, command in ipairs(hiddenCommands) do
        local cmdAction = cmds:action(command.name, command.aliases, command.description, command.action)
        if cmdAction then
            menu.set_visible(cmdAction, false)
        end
    end
end

registerCommands()