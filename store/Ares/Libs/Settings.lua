-- Settings for Ares

DebugModules = false
DebugExtra = false
RiskyMoney = false
PlayerOverlay = true

local notificationConfig = {
    {title = "Width", ref = {"notificationwidth"}, desc = "Width of notifications", min = 1, max = 12, default = 15, step = 1, multiplier = 0.01, assignFunc = function(value) notification.notif_width = value end},
    {title = "Title Size", ref = {"notificationtitlesize"}, desc = "Title size of notifications", min = 0, max = 10, default = 6, step = 1, multiplier = 0.1, assignFunc = function(value) notification.notif_title_size = value end},
    {title = "Text Size", ref = {"notificationtextsize"}, desc = "Text size of notifications", min = 0, max = 10, default = 5, step = 1, multiplier = 0.1, assignFunc = function(value) notification.notif_text_size = value end},
    {title = "Max Notifications", ref = {"maxnotifications"}, desc = "Maximum amount of notifications", min = 1, max = 10, default = 10, step = 1, multiplier = 1, assignFunc = function(value) notification.max_notifs = value end},
    {title = "Notification Spacing", ref = {"notificationspacing"}, desc = "Spacing between notifications", min = 0, max = 12, default = 15, step = 1, multiplier = 0.001, assignFunc = function(value) notification.notif_spacing = value end},
}

local function create_slider(root, title, ref, desc, min, max, default, step, isFloat, multiplier, assignFunc)
    if isFloat then
        menu.slider_float(root, title, ref, desc, min, max, default, step, function(value)
            assignFunc(value * multiplier)
        end)
    else
        menu.slider(root, title, ref, desc, min, max, default, step, function(value)
            assignFunc(value * multiplier)
        end)
    end
end

local Settings = menu.my_root():list("Settings", {""})

Settings:toggle("Show Risky Options", {}, "Save this, then re-start the script to access them", function(toggled) RiskyMoney = toggled end)
Settings:toggle("Player Overlay", {}, "This is a toggle for using Ares Player Overlay or not", function(toggled) PlayerOverlay = toggled end, true)

Settings:action("Open Ares Folder", {""}, "This stores Blacklist, User Blacklist & Player History", function()
    util.open_folder(AresFolder)
end)

if async_http.have_access() and !developer() then
    local Updater = Settings:list("Updater Options")
    Settings:action("Check for Updates", {"checkforupdate", "aresupdate"}, "", function()
        auto_update_config.check_interval = 0 notification.notify(scriptname, "Checking for updates")
        if auto_updater.run_auto_update(auto_update_config) then
            notification.notify(scriptname, "No updates have been found.", true)
        end
    end)
    Settings:action("Clean Reinstall", {}, "Force an update to the latest version, regardless of current version.", function()
        auto_update_config.clean_reinstall = true
        auto_updater.run_auto_update(auto_update_config)
    end)
end

local Debug = Settings:list("Debug Options", {""}, "Extra info such as error messages & reasoning for failure")
Debug:toggle("Feature Debug", {""}, "Enables Feature info with features", function(toggled) DebugExtra = toggled end)
Debug:toggle("Module Debug", {""}, "Enables debugging of ModuleHandler, i.e better error messages", function(toggled) DebugModules = toggled end)

local Notif = Settings:list("Notification Settings", {""})

for _, config in ipairs(notificationConfig) do
    create_slider(Notif, config.title, config.ref, config.desc, config.min, config.max, config.default, config.step, false, config.multiplier, config.assignFunc)
end

Notif:toggle("Use Default Notifications", {"notificationusedefault"}, "toggles between the custom and default notfications", function(value) notification.use_toast = value end)

local num = 1
Notif:action("Test Notification", {"testnotif"}, "", function()
   notification.notify(scriptname, $"This is a test notification +{num}", true) num = num + 1
end)

local num = 1
Notif:toggle_loop("Test Notification", {"testnotifloop"}, "", function()
	notification.notify(scriptname, $"This is a test notification +{num}", true) num = num + 1
	yield(2, "s")
end)

local banner = Notif:list("Banner Colour")
banner:colour("Colour", {"textcolour"}, "", notification.notif_banner_colour, true, function(colour)
    notification.notif_banner_colour = colour
end):rainbow()