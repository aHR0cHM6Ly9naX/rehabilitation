-- @pluto_warnings: disable-var-shadow

local function SelfProtections(Self)
    Self:toggle_loop("Block Electric Damage", {}, "Blocks electric damage from stunguns or electrical objects.", function()
        SET_PED_CONFIG_FLAG(players.user_ped(), 461, true)
        yield(1, "s")
    end, function()
        SET_PED_CONFIG_FLAG(players.user_ped(), 461, false)
    end)
    Self:action("Remove Attachments", {"remove attachments"}, "Cleans your ped of all attachments by regenerating it", function()
        notification.notify(scriptname, "Removing attachments")
        local Male = GET_ENTITY_MODEL(PLAYER_PED_ID()) == util.joaat('mp_m_freemode_01')
        if Male then
            RefWrapper("Self>Appearance>Transform>Playable Characters>Online Male")
        else
            RefWrapper("Self>Appearance>Transform>Playable Characters>Online Female")
        end
    end)
end

local function CombatProtections(Combat)
    local IsUsingOrbitalCannon = function(pid)
        return BitTest(memory.read_int(memory.script_global(2657971 + 1 + (pid * 465) + 426)), 0)
    end

    Combat:toggle_loop("Disable Yacht Defences", {""}, "Bypasses yacht defenses so you can still shoot while they're on", function()
        local val = memory.read_int(memory.script_global(262145 + memory.tunable_offset("YACHT_DISABLE_DEFENSES")))
        if val ~= 1 then
            INT_GLOBAL("YACHT_DISABLE_DEFENSES", 1)
        end
    end)

    Combat:toggle_loop("Ghost Godmode Players", {""}, "Ghost players who are in godmode", function()
        for pid = 0, max_players do
            if players.exists(pid) and pid ~= players.user() then
                local isGodMode = players.is_godmode(pid)
                local isInInterior = players.is_in_interior(pid)
                SET_REMOTE_PLAYER_AS_GHOST(pid, isGodMode and not isInInterior)
            end
        end
    end)

    Combat:toggle_loop("Orbital Cannon", {""}, "Detect players using the orbital cannon", function()
        for pid in players.list_except(true) do
            if IsUsingOrbitalCannon(pid) and GET_IS_TASK_ACTIVE(GET_PLAYER_PED_SCRIPT_INDEX(pid), 135) then
                util.draw_debug_text($"{players.get_name(pid)} is at the orbital cannon")
            end
        end
    end)

    Combat:toggle_loop("Anti-Orbital Cannon", {""}, "Activate anti-orbital cannon protection", function()
        for pid = 0, 31 do
            if players.exists(pid) and pid ~= players.user() then
                SET_REMOTE_PLAYER_AS_GHOST(pid, IsUsingOrbitalCannon(pid))
            end
        end
    end)

    Combat:toggle_loop("Anti-BST", {""}, "Normalize BST effects", function()
        for pid = 0, max_players do
            if players.exists(pid) and pid ~= players.user() then
                local pedPtr = entities.handle_to_pointer(GET_PLAYER_PED(pid))
                if pedPtr ~= 0 then
                    local playerInfo = entities.get_player_info(pedPtr)
                    if playerInfo and memory.read_float(playerInfo + 0x0D60) < 0.99607843160629 then
                        memory.write_float(playerInfo + 0x0D60, 0.99607843160629)
                    end
                end
            end
        end
    end)

    Combat:toggle_loop("Anti-Bounty", {""}, "Automatically remove any bounties set on you", function()
        local bounty = players.get_bounty(players.user())
        if bounty != nil then
            repeat
                RefWrapper("Online>Remove Bounty")
                yield(1, "s")
                bounty = players.get_bounty(players.user())
            until bounty == nil
            notification.notify(scriptname, "Bounty has been auto-claimed.")
        end
        util.yield(1000)
    end)
end

local function VehicleProtections(Vehicle)
    local ignoreEntities = {}
    Vehicle:toggle_loop("Anti-Oppressor MKII", {""}, "Delete broomsticks before they are used", function()
        for _, ent in ipairs(entities.get_all_vehicles_as_pointers()) do
            if entities.get_model_hash(ent) == util.joaat("oppressor2") and not ignoreEntities[ent] then
                entities.delete_by_pointer(ent)
                ignoreEntities[ent] = true
            end
        end
    end)

    Vehicle:toggle_loop("Block Sticky Bombs", {""}, "Auto remove sticky bombs from Car", function()
        if IS_PED_IN_ANY_VEHICLE(players.user_ped()) then
            local vehicle = entities.get_user_vehicle_as_handle()
            REMOVE_ALL_STICKY_BOMBS_FROM_ENTITY(vehicle, 0)
        end
    end)

    Vehicle:toggle_loop("Block NPC Vehicle Dragout", {""}, "Prevents NPCs from pulling you out of your vehicle", function()
        if IS_PED_IN_ANY_VEHICLE(players.user_ped()) then
            for _, ped in ipairs(entities.get_all_peds_as_handles()) do SET_PED_CONFIG_FLAG(ped, 342, true) end
        end
    end)

    Vehicle:toggle("Block Player Vehicle Dragout", {""}, "Blocks players from pulling you out of your vehicle", function(on)
        if IS_PED_IN_ANY_VEHICLE(players.user_ped()) then SET_PED_CONFIG_FLAG(players.user_ped(), 398, on) end
    end)
end

local function ScriptProtections(Session)
    Session:toggle_loop("Block Muggers", {""}, "Prevents you from being mugged.", function()
        if NETWORK_IS_SCRIPT_ACTIVE("am_gang_call", 0, true, 0) then
            local pedNetId = memory.script_local("am_gang_call", 73)
            local sender = memory.script_local("am_gang_call", 286)
            local target = memory.script_local("am_gang_call", 287)
            util.spoof_script("am_gang_call", function()
                local senderId = memory.read_int(sender)
                local targetId = memory.read_int(target)
                local pedId = memory.read_int(pedNetId)
                if senderId ~= players.user() and targetId == players.user() and NETWORK_DOES_NETWORK_ID_EXIST(pedId) and NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(pedId) then
                    local mugger = NET_TO_PED(pedId)
                    entities.delete(mugger)
                    notification.notify(scriptname, $"Blocked mugger from {players.get_name(senderId)}")
                end
            end)
        end
    end)

    Session:toggle_loop("Block Gooch Event", {""}, "Blocks the event that sends the gooch after you.", function()
        if GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(util.joaat("fm_content_xmas_mugger")) > 0 then
            local launcherHost = NETWORK_GET_HOST_OF_SCRIPT("am_launcher", -1, 0)
            TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("fm_content_xmas_mugger")
            if DebugExtra then
                notification.notify(scriptname, $"Blocked Gooch event, likely triggered by {launcherHost}")
            end
        end
    end)

    Session:toggle_loop("Block Hunt The Beast", {}, "Auto stops the Hunt the Beast script", function()
        if GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(util.joaat("am_hunt_the_Beast")) > 0 then
            TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("am_hunt_the_Beast")
            if DebugExtra then
                notification.notify(scriptname, "Blocked Hunt the Beast script")
            end
        end
    end)

    Session:toggle_loop("Block Destroy Vehicle Script", {""}, "Prevents the destroy vehicle script from being used maliciously to trigger transaction errors.", function()
        util.spoof_script("am_destroy_veh", TERMINATE_THIS_THREAD)
    end)
end

local function AudioProtections(Audio)
    local function stopAllSounds()
        for i = -1, 100 do
            STOP_SOUND(i)
            RELEASE_SOUND_ID(i)
        end
    end

    local function releasePedHandle(ped)
        if ped ~= players.user_ped() and ped ~= spawned_ped then
            RELEASE_SCRIPT_GUID_FROM_ENTITY(ped)
        end
    end

    Audio:action("Force Stop all sound events", {"stopsounds"}, "", stopAllSounds)

    Audio:toggle("Dead Silence", {"deadsilence"}, "Disables footstep sounds.", function(toggle)
        SET_PED_FOOTSTEPS_EVENTS_ENABLED(players.user_ped(), not toggle)
    end)

    Audio:toggle_loop("Disable Freemode Music", {""}, "Disables freemode music.", function()
        if AUDIO_IS_MUSIC_PLAYING() and not NETWORK_IS_ACTIVITY_SESSION() then
            TRIGGER_MUSIC_EVENT("GLOBAL_KILL_MUSIC")
        end
    end)

    Audio:toggle("Disable Distant Sirens", {""}, "", function(toggle)
        DISTANT_COP_CAR_SIRENS(not toggle)
    end)

    Audio:toggle_loop("Disable Vehicle Audio", {""}, "Mutes other vehicles.", function()
        local userVehicle = entities.get_user_vehicle_as_handle()
        for _, vehicle in ipairs(entities.get_all_vehicles_as_handles()) do
            if vehicle ~= userVehicle then
                FORCE_USE_AUDIO_GAME_OBJECT(vehicle, "")
            end
            RELEASE_SCRIPT_GUID_FROM_ENTITY(vehicle)
        end
    end)

    Audio:toggle_loop("Disable Ped Speech", {""}, "Mutes peds.", function()
        for _, ped in ipairs(entities.get_all_peds_as_handles()) do
            if IS_ANY_SPEECH_PLAYING(ped) then
                STOP_CURRENT_PLAYING_SPEECH(ped)
            end
            releasePedHandle(ped)
        end
    end)
end

local function CombatDetections(Combat)
    Combat:toggle_loop("Bullshark Testosterone", {""}, "Notifies you if a player has collected BST.", function()
        local data = memory.alloc(56 * 8)
        local BST_EVENT_ID = -584633745
        for queue = 0, 2 do
            local num_events = GET_NUMBER_OF_EVENTS(queue)
            for index = 0, num_events - 1 do
                local event = GET_EVENT_AT_INDEX(queue, index)
                if event == 174 and GET_EVENT_DATA(queue, index, data, 54) then
                    if memory.read_int(data) == BST_EVENT_ID then
                        local playerID = memory.read_int(data + 8)
                        util.toast($"[Ares] {players.get_name(playerID)} has collected BST.")
                    end
                end
            end
        end
    end)
    
    local IsInOrbRoom, IsOutOfOrbRoom, IsAtOrbTable, IsNotAtOrbTable = {}, {}, {}, {}
    Combat:toggle("Orbital Cannon Usage", {""}, "Notifies you if a Player has entered the Orbital Cannon Room.", function()
        util.create_tick_handler(function()
            for _, pid in ipairs(players.list(false, true, true)) do
                local pos = players.get_position(pid)
                if pos.x > 323 and pos.y < 4834 and pos.y > 4822 and pos.z <= -59.36 then
                    if IsOutOfOrbRoom[pid] and !IsInOrbRoom[pid] then
                        util.toast($"[Ares] {players.get_name(pid)} has entered the orbital cannon room.")
                    end
                    if pos.x < 331 and pos.x > 330.40 and pos.y > 4830 and pos.y < 4830.40 and pos.z <= -59.36 then
                        if IsNotAtOrbTable[pid] and !IsAtOrbTable[pid] then
                            util.toast($"[Ares] {players.get_name(pid)} is calling an Orbital Strike!")
                        end
                        IsAtOrbTable[pid] = true
                        IsNotAtOrbTable[pid] = false
                    end
                    IsInOrbRoom[pid] = true
                    IsOutOfOrbRoom[pid] = false
                else
                    if IsInOrbRoom[pid] and !IsOutOfOrbRoom[pid] then
                        util.toast($"[Ares] {players.get_name(pid)} has left the orbital cannon room.")
                    end
                    IsAtOrbTable[pid] = false
                    IsInOrbRoom[pid] = false
                    IsOutOfOrbRoom[pid] = true
                    IsNotAtOrbTable[pid] = true
                end
            end
        end)
    end)
end

local function EntityDetections(Entity)
    local BlockEntities = {
        util.joaat('prop_ld_ferris_wheel'), util.joaat('p_spinning_anus_s'),
        util.joaat('prop_windmill_01'), util.joaat('prop_staticmixer_01'),
        util.joaat('prop_towercrane_02a'), util.joaat('des_scaffolding_root'),
        util.joaat('stt_prop_stunt_bowling_ball'), util.joaat('stt_prop_stunt_soccer_ball'),
        util.joaat('prop_juicestand'), util.joaat('stt_prop_stunt_jump_l')
    }
    local function contains(tbl, element)
        for _, value in pairs(tbl) do
            if value == element then
                return true
            end
        end
        return false
    end

    Entity:toggle_loop("Glitch Player/Forcefield", {""}, "Detects players using JinxScript glitch player feature", function()
        for _, ptr in ipairs(entities.get_all_objects_as_pointers()) do
            local model = entities.get_model_hash(ptr)
            if contains(BlockEntities, model) then
                if memory.read_long(ptr + 0x28) & 0x10000100000000 == 0 then
                    local owner = entities.get_owner(ptr)
                    if players.exists(owner) and owner ~= players.user() then
                        if !isDetectionPresent(owner, "Glitch Player/Forcefield") then
                            players.add_detection(owner, "Glitch Player/Forcefield", TOAST_ALL, 100)
                        end
                        entities.delete(ptr)
                    end
                end
            end
        end
        yield(100, "ms")
    end)
end

local function VehicleDetections(Vehicle)
    local vehThings = table.freeze{
        "brickade2", "hauler", "hauler2",
        "manchez3", "terbyte", "minitank",
        "rcbandito", "volatus", "supervolito",
        "supervolito2"
    }
    local function PlateDetection(menuName, plateText)
        Vehicle:toggle_loop($"{menuName} Vehicle", {""}, $"Detects people using spawned cars by {menuName}.", function()
            for _, pid in ipairs(players.list()) do
                local ped = GET_PLAYER_PED_SCRIPT_INDEX(pid)
                local vehicle = GET_VEHICLE_PED_IS_USING(ped)
                local hash = players.get_vehicle_model(pid)
                local currentPlateText = GET_VEHICLE_NUMBER_PLATE_TEXT(vehicle)
                local bitset = DECOR_GET_INT(vehicle, "MPBitset")
                local pegasusveh = DECOR_GET_BOOL(vehicle, "CreatedByPegasus")
                local personalveh = (DECOR_GET_INT(vehicle, "Player_Vehicle") ~= 0)
                for _, vehicleHash in ipairs(vehThings) do
                    if hash == util.joaat(vehicleHash) and bitset == 8 then
                        return
                    end
                end
                local function VehDetection(detectionType)
                    if !isDetectionPresent(pid, detectionType) then
                        players.add_detection(pid, detectionType, TOAST_ALL, 50)
                    end
                end
                local driver = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(vehicle, -1))
                if hash ~= 0 and !GET_IS_TASK_ACTIVE(ped, 160) and isNetPlayerOk(players.user(), false, true) ~= 0 and
                   players.get_name(driver) ~= "InvalidPlayer" and !pegasusveh and !personalveh and pid == driver and
                   !players.is_in_interior(pid) and currentPlateText == plateText then
                    VehDetection(pid, $"Spawned Vehicle ({menuName})")
                end
            end
        end)
    end
    PlateDetection("Lexis", " LEXIS  ")
end

local function EventDetections(Event)
    local function getPlayerJobPoints(pid)
        return memory.read_int(memory.script_global(1845281 + 1 + (pid * 883) + 9))
    end
    local function SEDetections(pid, detectionType)
        if !isDetectionPresent(pid, detectionType) then
            players.add_detection(pid, detectionType, TOAST_ALL, 50)
        end
    end

    Event:toggle("Net Event", {""}, "", function(on, click_type)
        if on then
            players.on_flow_event_done(function(pid, name, extra)
                name = lang.get_localised(name)
                if extra then name ..= " (" name ..= extra name ..= ")" end
                local function Detection(detectionType)
                    if !isDetectionPresent(pid, detectionType) then players.add_detection(pid, detectionType, TOAST_ALL, 50) end
                end
                if string.find(name, "REPORT_CASH_SPAWN_EVENT") then
                    Detection("Net Event(N0)")
                elseif string.find(name, "Give Weapon Event") then
                    Detection("Net Event(N1)")
                end
            end)
        end
    end)

    Event:toggle("ScriptEvents", {}, "Detects & Blocks various scriptevents\nS1 will false flag", function(state)
        util.create_tick_handler(function()
            for i = 0, GET_NUMBER_OF_EVENTS(1) - 1 do
                if GET_EVENT_AT_INDEX(1, i) == 174 then
                    local event = memory.alloc()
                    GET_EVENT_DATA(1, i, event, 4)
                    if memory.read_int(event) == 57493695 then
                        local pid = memory.read_int(event + 8)
                        SEDetections(pid, "Modded Scriptevent (S0)")
                    elseif memory.read_int(event) == -337848027 then
                        local pid = memory.read_int(event + 8)
                        SEDetections(pid, "Modded Scriptevent (S1)")
                    elseif memory.read_int(event) == -1704545346 then
                        local pid = memory.read_int(event + 8)
                        SEDetections(pid, "Modded Scriptevent (S2)")
                    end
                end
            end
        end)
        memory.script_global(1683951 + 510, state ? 1 : 0)
        memory.script_global(1683951 + 512, state ? 1 : 0)
        memory.script_global(1683951 + 561, state ? 1 : 0)
    end)
end

local function RegisterProtections()
    local Protections = menu.my_root():list("Protections", {}, "")
    local ProtectionItems = {
        {name = "Self", setupFunc = SelfProtections, hidden = false},
        {name = "Combat", setupFunc = CombatProtections, hidden = false},
        {name = "Vehicle", setupFunc = VehicleProtections, hidden = false},
        {name = "Session Scripts", setupFunc = ScriptProtections, hidden = false},
        {name = "Audio", setupFunc = AudioProtections, hidden = false},
    }
    for _, item in ipairs(ProtectionItems) do
        local success, result = pcall(function()
            local menuList = Protections:list(item.name, {}, item.desc or "")
            if item.setupFunc then
                item.setupFunc(menuList)
            end
            if item.hidden then
                menu.set_visible(menuList, false)
            end
        end)
        if !success then
            util.toast("[Ares] Caught an Exception during Protections setup", TOAST_DEFAULT | TOAST_CONSOLE)
            if DebugModules then util.log($"[Ares] {item.name}: {result}") end
        end
    end

    local DetectionItems = {
        {name = "Combat", setupFunc = CombatDetections, hidden = false},
        {name = "Entity", setupFunc = EntityDetections, hidden = false},
        {name = "Vehicle", setupFunc = VehicleDetections, hidden = false},
        {name = "Event", setupFunc = EventDetections, hidden = false},
    }
    local Detections = Protections:list("Detections", {}, "Detection options")
    for _, item in ipairs(DetectionItems) do
        local success, result = pcall(function()
            local menuList = Detections:list(item.name, {}, item.desc or "")
            if item.setupFunc then item.setupFunc(menuList) end
            if item.hidden then menu.set_visible(menuList, false) end
        end)
        if !success then
            util.toast("[Ares] Caught an Exception during Detections setup", TOAST_DEFAULT | TOAST_CONSOLE)
            if DebugModules then util.log($"[Ares] {item.name}: {result}") end
        end
    end
end

RegisterProtections()