local RES_X, RES_Y = 1920, 1080
local ASPECT_RATIO <const> = RES_X/RES_Y
local gui_x = 0
local gui_y = 0
local colour =
{
    title_bar = {r = 137/255, g = 0/255, b = 0/255, a = 255/255},
    background = {r = 0, g = 0, b = 0, a = 77/255},
    health_bar = {r = 114/255, g = 204/255, b = 114/255, a = 150/255},
    armour_bar = {r = 70/255, g = 136/255, b = 171/255, a = 150/255},
    map = {r = 255/255, g = 255/255, b = 255/255, a = 0.75},
    cmap = {r = 255/255, g = 255/255, b = 255/255, a = 195/255},
    blip = {r = 255/255, g = 255/255, b = 255/255, a = 255/255},
    name = {r = 255/255, g = 255/255, b = 255/255, a = 255/255},
    label = {r = 255/255, g = 255/255, b = 255/255, a = 255/255},
    info = {r = 255/255, g = 255/255, b = 255/255, a = 255/255},
    border = {r = 0/255, g = 0/255, b = 0/255, a = 255/255}
}
local MapToggle = false
local name_h = 24/RES_Y
local padding = 8/RES_Y
local spacing = 3/RES_Y
local gui_w = 290/RES_X
local bar_w_mult = 1.8
local blip_size = 0.0035
local name_size = 0.52
local text_size = 0.41
local line_spacing = 10/RES_Y
local border_width = 0
local blur_strength = 4
local blur = {}
for i = 1, 8 do blur[i] = directx.blurrect_new() end
local textures =
{
    map = directx.create_texture(filesystem.resources_dir() .. "Ares\\Overlay\\Map.png"),
    map2 = directx.create_texture(filesystem.resources_dir() .. "Ares\\Overlay\\Map2.png"),
    blip = directx.create_texture(filesystem.resources_dir() .. "Ares\\Overlay\\Blip.png")
}
local render_window = false
local AresOverlay = menu.attach_before(menu.ref_by_path("Players>Settings>Tags"), menu.list(menu.shadow_root(), "Ares Overlay Settings", {}, "", 
function()
    render_window = true 
end, 
function()
    render_window = false
end))
AresOverlay:divider("Position")
AresOverlay:slider("X:", {"aOverlayx"}, "Horizontal position of the info overlay.", 0, RES_X, 0, 1, function(s)
    gui_x = s/RES_X
end)
AresOverlay:slider("Y:", {"aOverlayy"}, "Vertical position of the info overlay.", 0, RES_Y, 0, 1, function(s)
    gui_y = s/RES_Y
end)

--appearance divider
AresOverlay:divider("Appearance")

local Settings = AresOverlay:list("Info", {}, "Toggles & Settings")

local MapOptions = {"Black & White", "Colored Map"}

--Settings:toggle("Player Preview", {""}, "", function(toggled) PlayerToggle = toggled end)
Settings:toggle("Render Map", {""}, "", function(toggled) MapToggle = toggled end)

local BlackAndWhite = true
local ColouredMap = false

local map_options = {"Black & White", "Colored Map"}

local map_option = Settings:list_select("Map", {}, "", map_options, 1, function(index, name)
    BlackAndWhite = false
    ColouredMap = false
    if index == 1 then
        BlackAndWhite = true
    elseif index == 2 then
        ColouredMap = true
    end
end)

--set colours
local colours = AresOverlay:list("Overlay colours", {}, "")

colours:divider("Elements")

colours:colour("Title Bar Colour", {"aOverlaytitle_bar"}, "Colour of the title bar.", colour.title_bar, true, function(on_change)
    colour.title_bar = on_change
end)
colours:colour("Background Colour", {"aOverlaybg"}, "Colour of the background.", colour.background, true, function(on_change)
    colour.background = on_change
end)
colours:colour("Health Bar Colour", {"aOverlayhealth_bar"}, "Colour of the health bar.", colour.health_bar, true, function(on_change)
    colour.health_bar = on_change
end)
colours:colour("Armour Bar Colour", {"aOverlayarmour_bar"}, "Colour of the armour bar.", colour.armour_bar, true, function(on_change)
    colour.armour_bar = on_change
end)
colours:colour("Map Colour", {"aOverlaymap"}, "Colour of the map.", colour.map, true, function(on_change)
    colour.map = on_change
end)
colours:colour("Blip Colour", {"aOverlayblip"}, "Colour of the map blip.", colour.blip, true, function(on_change)
    colour.blip = on_change
end)

colours:divider("Text")
colours:colour("Name Colour", {"aOverlayname"}, "Colour of the player name text.", colour.name, true, function(on_change)
    colour.name = on_change
end)
colours:colour("Label Colour", {"aOverlaylabel"}, "Colour of the label text.", colour.label, true, function(on_change)
    colour.label = on_change
end)
colours:colour("Info Colour", {"aOverlayinfo"}, "Colour of the info text.", colour.info, true, function(on_change)
    colour.info = on_change
end)

--set element sizing & spacing
local element_dim = AresOverlay:list("Element Sizing & Spacing", {}, "")

element_dim:divider("Element Sizing & Spacing")

element_dim:slider("Title Bar Height", {}, "Height of the title bar.", 0, math.floor(3 * name_h * RES_Y), math.floor(name_h * RES_Y), 1, function(on_change)
    name_h = on_change/RES_Y
end)
element_dim:slider("Overlay Width", {"aOverlaywidth"}, "Width of the text window minus the padding.", 0, math.floor(3 * gui_w * RES_Y), math.floor(gui_w * RES_Y), 10, function(on_change)
    gui_w = on_change/RES_Y
end)
element_dim:slider("Padding", {}, "Padding around the info text.", 0, math.floor(3 * padding * RES_Y), math.floor(padding * RES_Y), 1, function(on_change)
    padding = on_change/RES_Y
end)
element_dim:slider("Spacing", {}, "Spacing of the different elements.", 0, math.floor(5 * spacing * RES_Y), math.floor(spacing * RES_Y), 1, function(on_change)
    spacing = on_change/RES_Y
end)
element_dim:slider_float("Bar Width Multiplier", {}, "Multiplier for the width of the health and armour bar.", 0, math.floor(3 * bar_w_mult * 100), math.floor(bar_w_mult * 100), 10, function(on_change)
    bar_w_mult = on_change/100
end)
element_dim:slider_float("Blip Size", {}, "Size of the map blip.", 0, math.floor(3 * blip_size * 10000), math.floor(blip_size * 10000), 1, function(on_change)
    blip_size = on_change/10000
end)

--set text sizing & spacing
local text_dim = AresOverlay:list("Text Sizing & Spacing", {}, "")

text_dim:divider("Text Sizing & Spacing")

text_dim:slider_float("Name", {}, "Size of the player name text.", 0, 100, math.floor(name_size * 100), 1, function(on_change)
    name_size = on_change/100
end)
text_dim:slider_float("Info Text", {}, "Size of the info text.", 0, 100, math.floor(text_size * 100), 1, function(on_change)
    text_size = on_change/100
end)
text_dim:slider("Line Spacing", {}, "Spacing inbetween lines of info text.", 0, math.floor(3 * line_spacing * RES_Y), math.floor(line_spacing * RES_Y), 1, function(on_change)
    line_spacing = on_change/RES_Y
end)

local border = AresOverlay:list("Border", {}, "")

border:divider("Border Settings")

border:slider("Width", {}, "Width of the border rendered around the elements.", 0, math.floor(3 * spacing * RES_Y), 0, 1, function(on_change)
    border_width = on_change/RES_Y
end)
local border_c_slider = border:colour("Colour", {"aOverlayborder"}, "Colour of the rendered border.", colour.border, true, function(on_change)
    colour.border = on_change
end)
border_c_slider:rainbow()

AresOverlay:slider("Background Blur", {}, "Amount of blur applied to background.", 0, 255, blur_strength, 1, function(on_change)
    blur_strength = on_change
end)

local function drawRect(x, y, w, h, colour)
    directx.draw_rect(x, y, w, h, 
    {r = colour.r * colour.a, g = colour.g * colour.a, b = colour.b * colour.a, a = colour.a})
end

local function drawBorder(x, y, width, height)
    local border_x = border_width/ASPECT_RATIO
    drawRect(x - border_x, y, width + border_x * 2, -border_width, colour.border) --top
    drawRect(x, y, -border_x, height, colour.border) --left
    drawRect(x + width, y, border_x, height, colour.border) --right
    drawRect(x - border_x, y + height, width + border_x * 2, border_width, colour.border) --bottom
end

local function roundNum(num, decimals)
    local mult = 10^(decimals or 0)
    return math.floor(num * mult + 0.5) / mult
end

local all_weapons = {}
local temp_weapons = util.get_weapons()
for a, b in pairs(temp_weapons) do
    all_weapons[#all_weapons + 1] = {hash = b["hash"], label_key = b["label_key"]}
end
local function hashToWeapon(hash) 
    for k, v in pairs(all_weapons) do 
        if v.hash == hash then 
            return util.get_label_text(v.label_key)
        end
    end
    return "Unarmed"
end

local function boolText(bool)
    if bool then return "Yes" else return "No" end
end

local function checkValue(pInfo)
    if pInfo == "" or pInfo == 0 or pInfo == nil or pInfo == "NULL" then return "None" else return pInfo end 
end

local function formatMoney(money)
    local order = math.ceil(string.len(tostring(money))/3 - 1)
    if order == 0 then return money end
    return roundNum(money/(1000^order), 1)..({"K", "M", "B"})[order]
end

local function ResolveIP(pid)
    local connectIP = players.get_ip(pid)
    if connectIP == 0 or connectIP == 0xFFFFFFFF then return "Connected via Relay" end
    local ipParts = {(connectIP >> 24) & 0xFF,(connectIP >> 16) & 0xFF,(connectIP >> 8) & 0xFF,connectIP & 0xFF}
    local ip = table.concat(ipParts, ".")
    local vpn = players.is_using_vpn(pid) and " (VPN)" or ""
    return ip .. vpn
end

local function get_ip_data(ip)
    local data = {city = "Unknown", state = "Unknown", country = "Unknown"}
    if util.is_soup_netintel_inited() then
        if (loc := soup.netIntel.getLocationByIp(ip)):isValid() then
            data.city = loc.city
            data.state = loc.state
            data.country = soup.getCountryName(loc.country_code, "EN")
        end
    end
    return data
end

function IOverlay()
        if true then
            local focused = players.get_focused()
            if ((focused[1] ~= nil and focused[2] == nil) or render_window) and menu.is_open() then
    
                --general info grabbing locals
                local pid = focused[1]
                if render_window then pid = players.user() end
                local ped = GET_PLAYER_PED_SCRIPT_INDEX(pid)
                local my_pos, player_pos = players.get_position(players.user()), players.get_position(pid)
                
                --general element drawing locals
                local spacing_x = spacing/ASPECT_RATIO
                local padding_x = padding/ASPECT_RATIO
                local player_list_y = gui_y + name_h + spacing
                local total_w = gui_w + padding_x * 4
    
                local heading = GET_ENTITY_HEADING(ped)
                local IP = tostring(soup.IpAddr(players.get_connect_ip(pid)))
                local regions = 
                {
                    {
                        width = total_w/2,
                        content =
                        {
                            {"Rank", players.get_rank(pid)},
                            {"K/D", roundNum(players.get_kd(pid), 2)},
                            {"Wallet", "$"..formatMoney(players.get_wallet(pid))},
                            {"Bank", "$"..formatMoney(players.get_bank(pid))}
                        }
                    },
                    {
                        width = total_w/2,
                        content =
                        {
                            {"Language", ({"English","French","German","Italian","Spanish","Brazilian","Polish","Russian","Korean","Chinese (T)","Japanese","Mexican","Chinese (S)"})[players.get_language(pid) + 1]},
                            {"Controller", boolText(players.is_using_controller(pid))},
                            {"Ping", math.floor(NETWORK_GET_AVERAGE_PING(pid) + 0.5).." ms"},
                            {"Host Queue", "#"..players.get_host_queue_position(pid)},
                        }
                    },
                    {
                        width = total_w + spacing_x,
                        content =
                        {
                            {"Model", util.reverse_joaat(GET_ENTITY_MODEL(ped))},
                            {"Zone", util.get_label_text(GET_NAME_OF_ZONE(player_pos.x, player_pos.y, player_pos.z))},
                            {"Weapon", hashToWeapon(GET_SELECTED_PED_WEAPON(ped))},
                            {"Vehicle", checkValue(util.get_label_text(players.get_vehicle_model(pid)))}
                        }
                    },
                    {
                        width = total_w/2,
                        content =
                        {
                            {"Distance", math.floor(GET_DISTANCE_BETWEEN_COORDS(player_pos.x, player_pos.y, player_pos.z, my_pos.x, my_pos.y, my_pos.z)).." m"},
                            {"Speed", math.floor(GET_ENTITY_SPEED(ped) * 3.6).." km/h"},
                            {"Going", ({"North","West","South","East","North"})[math.ceil((heading + 45)/90)]..", "..math.ceil(heading).."°"}
                        }
                    },
                    {
                        width = total_w/2,
                        content =
                        {
                            {"Organization", ({"None","CEO","MC"})[players.get_org_type(pid) + 2]},
                            {"Wanted", GET_PLAYER_WANTED_LEVEL(pid).."/5"},
                            {"Cutscene", boolText(IS_PLAYER_IN_CUTSCENE(pid))}
                        }
                    },
                    {
                        width = total_w + spacing_x,
                        content =
                        {
                            {"IP", ResolveIP(pid)},
                            {"City", get_ip_data(players.get_ip(pid)).city or ""},
                            {"Region", get_ip_data(players.get_ip(pid)).state or ""},
                            {"Country", get_ip_data(players.get_ip(pid)).country or ""}
                        }
                    },
                }
    
                local font_w, font_h = directx.get_text_size("ABCDEFG", text_size/ASPECT_RATIO)
                local offset_x = 0
                local offset_y = 0
                
                for k, region in ipairs(regions) do
                    local blur_instance = 1
                    local count = 0
                    for _ in region.content do count = count + 1 end
                    local dict_h = count * (font_h + line_spacing) + padding * 2
    
                    drawBorder(gui_x + offset_x, player_list_y + offset_y, region.width, dict_h)
                    directx.blurrect_draw(blur[blur_instance], gui_x + offset_x, player_list_y + offset_y, region.width, dict_h, blur_strength)
                    drawRect(gui_x + offset_x, player_list_y + offset_y, region.width, dict_h, colour.background)
    
                    local line_count = 0.5
                    for _, v in ipairs(region.content) do
                        directx.draw_text(
                        gui_x + offset_x + padding_x - 0.001, 
                        player_list_y + offset_y + padding + line_count * (font_h + line_spacing), 
                        v[1]..": ",
                        ALIGN_CENTRE_LEFT, 
                        text_size, 
                        colour.label
                        )
                        directx.draw_text(
                        gui_x + offset_x + region.width - padding_x - 0.001, 
                        player_list_y + offset_y + padding + line_count * (font_h + line_spacing), 
                        v[2], 
                        ALIGN_CENTRE_RIGHT, 
                        text_size, 
                        colour.info
                        )
                        line_count += 1
                    end
    
                    offset_x += region.width + spacing_x
                    if offset_x >= total_w then
                        offset_y += dict_h + spacing
                        offset_x = 0
                    end
                    blur_instance += 1
                end
    
                local MAP_RATIO = 902/594
                local gui_h = offset_y - spacing
                local map_x = gui_x + total_w + spacing_x * 2
                local map_w = gui_h/MAP_RATIO/ASPECT_RATIO + 0.001
                local bar_w = gui_h/50/ASPECT_RATIO * bar_w_mult
                local map_w_total = map_w + padding_x * 3 + bar_w

                --[[ NAME BAR ]]
    
                drawBorder(gui_x, gui_y, total_w + spacing_x, name_h)
    
                directx.blurrect_draw(blur[6], gui_x, gui_y, total_w, name_h, blur_strength)
    
                drawRect(gui_x, gui_y, total_w + spacing_x, name_h, colour.title_bar)
    
                directx.draw_text(gui_x + total_w/2, gui_y + name_h/2, players.get_name(pid), ALIGN_CENTRE, name_size, colour.name)

                if MapToggle then
                    if BlackAndWhite then
                        drawBorder(map_x, gui_y, map_w_total, name_h)
                        directx.blurrect_draw(blur[7], map_x, gui_y, map_w_total, name_h, blur_strength)
                        drawRect(map_x, gui_y, map_w_total, name_h, colour.title_bar)
                        drawBorder(map_x, player_list_y, map_w_total, gui_h)
                        directx.blurrect_draw(blur[8], map_x, player_list_y, map_w_total, gui_h, blur_strength)
                        drawRect(map_x, player_list_y, map_w_total, gui_h, colour.background)
                        directx.draw_texture(textures.map, map_w/2, gui_h, 0.5, 0.5, map_x + padding_x * 2 + bar_w + map_w/2 , player_list_y + gui_h/2, 0, colour.map)
                    elseif ColouredMap then
                        drawBorder(map_x, gui_y, map_w_total, name_h)
                        directx.blurrect_draw(blur[7], map_x, gui_y, map_w_total, name_h, blur_strength)
                        drawRect(map_x, gui_y, map_w_total, name_h, colour.title_bar)
                        drawBorder(map_x, player_list_y, map_w_total, gui_h)
                        directx.blurrect_draw(blur[8], map_x, player_list_y, map_w_total, gui_h, blur_strength)
                        drawRect(map_x, player_list_y, map_w_total, gui_h, colour.background)
                        directx.draw_texture(textures.map2, map_w/2, gui_h, 0.5, 0.5, map_x + padding_x * 2 + bar_w + map_w/2 , player_list_y + gui_h/2, 0, colour.cmap)
                    end
                end
                --blip
                local blip_dx = ((player_pos.x + 3745)/8316) * map_w
                local blip_dy = (1 - (player_pos.y + 4427)/12689) * gui_h
                if MapToggle then
                    directx.draw_texture(textures.blip, blip_size, blip_size, 0.5, 0.5, map_x + padding_x * 2 + bar_w + blip_dx, player_list_y + blip_dy, (360 - heading)/360, colour.blip)
                end
                if MapToggle then
                    local armour_perc = GET_PED_ARMOUR(ped)/GET_PLAYER_MAX_ARMOUR(pid)
                    local armour_bar_bg = {r = colour.armour_bar.r/2, g = colour.armour_bar.g/2, b = colour.armour_bar.b/2, a = colour.armour_bar.a}
                    drawRect(map_x + padding_x, player_list_y + gui_h/2 - padding/2, bar_w, -((gui_h - padding * 3)/2 * armour_perc), colour.armour_bar) --foreground
                    drawRect(map_x + padding_x, player_list_y + padding, bar_w, (gui_h - padding * 3)/2 * (1 - armour_perc), armour_bar_bg) --background
                    local health_min = GET_ENTITY_HEALTH(ped) - 100
                    if health_min < 0 then health_min = 0 end
                    local health_perc = health_min/(GET_ENTITY_MAX_HEALTH(ped) - 100)
                    local health_bar_bg = {r = colour.health_bar.r/2, g = colour.health_bar.g/2, b = colour.health_bar.b/2, a = colour.health_bar.a}
                    drawRect(map_x + padding_x, player_list_y + gui_h - padding, bar_w, -((gui_h - padding * 3)/2 * health_perc), colour.health_bar) --foreground
                    drawRect(map_x + padding_x, player_list_y + gui_h/2 + padding/2, bar_w, (gui_h - padding * 3)/2 * (1 - health_perc), health_bar_bg) --background
                end
            end
        end
end