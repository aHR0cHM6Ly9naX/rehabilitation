-- @pluto_warnings: disable-var-shadow

local GlobalplayerBD = 2657921
local GlobalplayerBD_FM = 1845263
local GlobalplayerBD_FM_3 = 1886967

local Dev = developer()

local function SET_INT_LOCAL(script, script_local, value)
    if memory.script_local(script, script_local) ~= 0 then
        memory.write_int(memory.script_local(script, script_local), value)
    end
end

local function FX(asset)
    while !HAS_NAMED_PTFX_ASSET_LOADED(asset) do
        REQUEST_NAMED_PTFX_ASSET(asset)
        yield()
    end
    USE_PARTICLE_FX_ASSET(asset)
end

local function SpawnPed(model_name, pos, gm = false): ?int
    local hash = util.joaat(model_name)
    if IS_MODEL_A_PED(hash) then
        util.request_model(hash)
        local ped = entities.create_ped(2, hash, pos, GET_FINAL_RENDERED_CAM_ROT(2).z)
        SET_ENTITY_INVINCIBLE(ped, gm)
        local ptr = entities.handle_to_pointer(ped)
        entities.set_can_migrate(ptr, false)
        SET_MODEL_AS_NO_LONGER_NEEDED(hash)
        return ped
    else
        return nil, notification.notify(scriptname, $"{model_name} is not a valid ped.")
    end
end

local function SpawnVehicle(model_name, pos, gm = false): ?int
    local hash = util.joaat(model_name)
    if IS_MODEL_A_VEHICLE(hash) then
        util.request_model(hash)
        local veh = entities.create_vehicle(hash, pos, GET_FINAL_RENDERED_CAM_ROT(2).z)
        local ptr = entities.handle_to_pointer(veh)
        SET_ENTITY_INVINCIBLE(veh, gm)
        entities.set_can_migrate(ptr, false)
        SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(veh, true)
        SET_MODEL_AS_NO_LONGER_NEEDED(hash)
        return veh
    else
        return nil, notification.notify(scriptname, $"{model_name} is not a valid vehicle.")
    end
end

enum ScriptEvents begin
    Invalid = 323285304, Crash = -1604421397, Kick = -901348601,
    Collectible = 968269233, Challenge = 1450115979, WarpInside = -1638522928, 
    LaunchSync = -1604421397, ForceMission = -366707054, Confirmation = 1757622014
end

enum Tags begin
    SELF = -1974706693, BLOCKED = -748077967, TOOFAST = 1669138996, 
    NOVEH = 1067523721, HOST = 2121500616, ONHOST = -569541965
end

enum eDamageFlags begin
    DF_None                             = 0, DF_IsAccurate                       = 1,
    DF_MeleeDamage                      = 2, DF_SelfDamage                       = 4,
    DF_ForceMeleeDamage                 = 8, DF_IgnorePedFlags                   = 16,
    DF_ForceInstantKill                 = 32, DF_IgnoreArmor                      = 64,
    DF_IgnoreStatModifiers              = 128, DF_FatalMeleeDamage                 = 256,
    DF_AllowHeadShot                    = 512, DF_AllowDriverKill                  = 1024,
    DF_KillPriorToClearedWantedLevel    = 2048, DF_SuppressImpactAudio              = 4096,
    DF_ExpectedPlayerKill               = 8192, DF_DontReportCrimes                 = 16384,
    DF_PtFxOnly                         = 32768, DF_UsePlayerPendingDamage           = 65536,
    DF_AllowCloneMeleeDamage            = 131072, DF_NoAnimatedMeleeReaction          = 262144,
    DF_IgnoreRemoteDistCheck            = 524288, DF_VehicleMeleeHit                  = 1048576,
    DF_EnduranceDamageOnly              = 2097152, DF_HealthDamageOnly                 = 4194304,
    DF_DamageFromBentBullet             = 8388608
end

local function setupBlacklist(Blacklist, pid)
    local rid = players.get_rockstar_id(pid)

    local function InBlacklist(rid)
        local id = tostring(rid)
        return BL_Table[id] ~= nil
    end
    
    local function BlocknKick(Block, Kick)
        if Block then RefWrapper($"Online>Player History>{players.get_name(pid)}>Player Join Reactions>Block Join") end
        if Kick then
            OnlineRefWrapper(menu.player_root(pid), "Kick>Love Letter")
            yield(15, "s")
            if players.exists(pid) then return notification.notify(scriptname, $"Love Letter Failed on {players.get_name(pid)}") end
            if players.get_host() == pid then return notification.notify(scriptname, lang.get_localised(ONHOST)) end
        end
    end

    local function AddToBlacklist(rid)
        if rid ~= players.get_rockstar_id(players.user()) then
            local id = tostring(rid)
            data_e[id] = true
            BL_Table[id] = true
            SaveDataE()
        else
            notification.notify(scriptname, "Fucking brain issue?\nYou can't blacklist yourself", true)
        end
    end

    Blacklist:action("Add Blacklist Only", {""}, "Block the Target from Joining u again.", function ()
        BlocknKick(true, false)
        if !InBlacklist(rid) then
            AddToBlacklist(rid)
        end
    end)

    Blacklist:action("Add Blacklist & Kick", {""}, "Attempts to love letter kick and Block the Target from Joining u again.", function ()
        BlocknKick(true, true)
        if !InBlacklist(rid) then
            AddToBlacklist(rid)
        end
    end)
end

local function setupKick(Kicks, pid)
    local function KickHandler(Kick, EditionCheck, ModderCheck, GodCheck, HostCheck)
        if EditionCheck and !isEditionValid(EditionCheck) then notification.notify(scriptname, "Preventing kick\nReason: Wrong Edition") return end
        if ModderCheck and players.is_marked_as_modder(pid) then notification.notify(scriptname, "Preventing kick\nReason: Modder") return end
        if GodCheck and players.is_godmode(pid) then notification.notify(scriptname, "Preventing kick\nReason: Godmode") return end
        if HostCheck and Host_Check() then notification.notify(scriptname, lang.get_localised(HOST)) return end
        if pid == players.user() then notification.notify(scriptname, lang.get_localised(SELF)) return end
        Kick()
    end
    Kicks:action("Kill Freemode", {"fm", "kfm", "freemode"}, "Kills the player's freemode, Sending them back to story", function()
        KickHandler(function()
            util.trigger_script_event(1 << pid, {Collectible, players.user(), 8192, 4, -1, 1, 1})
            util.trigger_script_event(1 << pid, {util.joaat("Globals.MP_Event_Enums15.sch.SCRIPT_EVENT_GB_NON_BOSS_CHALLENGE_REQUEST"), players.user(), math.random(311, 330), -1})
            util.trigger_script_event(1 << pid, {305758732, players.user(), math.random(-2147483647, 2147483647), math.random(-2147483647, 2147483647), math.random(-2147483647, 2147483647), math.random(-2147483647, 2147483647), math.random(-2147483647, 2147483647), math.random(-2147483647, 2147483647)})
        end, 1, true, false, false)
    end)
    Kicks:action("Kick Car", {}, "Spawns a car for the player to get in, will result in freemode death for the lobby", function()
        local pos = players.get_position(pid)
        local KickCar = SpawnVehicle("Adder", pos, true)
        yield(150)
        DECOR_SET_INT(KickCar, "ContrabandOwner", -2147483647)
    end)
    local tb = Kicks:list("Ban Kicks")
    local KickTable = table.freeze{
        {name = "Ban Kick", cmd = {"ban", "tban", "tb"}, desc = "Kicks the player and blacklists them.", msg = $"{players.get_name(pid)} was detected for malicious behavior: They were removed"},
        {name = "Remove for Menu Abuse", cmd = {""}, desc = "blacklists the player and sends menu abuse in chat.", msg = $"{players.get_name(pid)} was removed from the session. Reason: menu Abuse (U1)"},
        {name = "Remove for Block Join", cmd = {""}, desc = "blacklists the player and sends block join in chat.", msg = $"{players.get_name(pid)} was removed from the session. Reason: block Join (U1)"},
        {name = "Remove for Detected", cmd = {""}, desc = "blacklists the player and sends detected in chat.", msg = $"{players.get_name(pid)} was removed from the session. Reason: Detected (U1)"},
        {name = "Remove for RAC", cmd = {""}, desc = "blacklists the player and sends removed for RAC in chat.", msg = $"{players.get_name(pid)} was removed from the session. Reason: Menu Detected by the Anti-Cheat (R0)"},
        {name = "Remove for Godmode", cmd = {""}, desc = "blacklists the player and sends removed for Godmode in chat.", msg = $"{players.get_name(pid)} was removed from the session. Reason: Godmode Abuse (GA)"}
    }
    local function KickMessages(entry)
        tb:action(entry.name, entry.cmd, entry.desc or "", function()
            KickHandler(function()
                chat.send_message(entry.msg, false, true, true)
                yield(500, "ms")
                OnlineRefWrapper(menu.player_root(pid), "Kick>Blacklist")
            end, 1, false, false, true)
        end)
    end
    for _, entry in ipairs(KickTable) do KickMessages(entry) end
    Kicks:toggle_loop("Kick on Attack", {""}, "Auto kick if they atack you.", function()
        if !menu.player_root(pid):isValid() then return end
        if players.is_marked_as_attacker(pid) then
            OnlineRefWrapper(menu.player_root(pid), "Kick>Smart")
            notification.notify(scriptname, $"Attempting to kick {players.get_name(pid)} bcs they atacked you.")
        end
    end)
    Kicks:action("Block Joins & Kick", {"blockjoin", "bk"}, "Adds them to your block history join then smart Kicks them.", function()
        KickHandler(function()
            RefWrapper($"Online>Player History>{players.get_name(pid)}>Player Join Reactions>Block Join")
            yield(2, "ms")
            OnlineRefWrapper(menu.player_root(pid), "Kick>Smart")
        end, 1, false, false, true)
    end)
end

local function setupCrash(Crashes, pid)
    local function CrashHandler(pid, Crash)
        if pid == players.user() then notification.notify(scriptname, lang.get_localised(SELF)) return end
        if !players.exists(pid) then return end
        Crash(pid)
    end
    local function deleteEntitiesAfterDelay(entities, delay)
        util.yield(delay)
        for _, entity in ipairs(entities) do
            if DOES_ENTITY_EXIST(entity) and NETWORK_HAS_CONTROL_OF_ENTITY(entity) then
                entities.delete_by_handle(entity)
            else
                if NETWORK_REQUEST_CONTROL_OF_ENTITY(entity) then
                    util.yield(500)
                    if DOES_ENTITY_EXIST(entity) then
                        entities.delete_by_handle(entity)
                    end
                end
            end
        end
    end
    Crashes:action("Smart Crash", {"sc", "scrash", "cs"}, "Attempts to crash them then 5-10 seconds later kicks them", function()
        CrashHandler(pid, function(pid)
            OnlineRefWrapper(menu.player_root(pid), "Ares>Removal>Crash>Femboy Crash")
            yield(15, "s")
            if players.exists(pid) then
                if players.get_host(pid) then
                    OnlineRefWrapper(menu.player_root(pid), "Kick>Host")
                else
                    OnlineRefWrapper(menu.player_root(pid), "Kick>Love Letter")
                end
            end
        end)
    end)
    Crashes:action("Orbital Crash", {"orb", "oc"}, $"{lang.get_localised(BLOCKED)}\nOrbital cannons the player then triggers crashes on them", function()
        CrashHandler(pid, function(pid)
            local playerPed = GET_PLAYER_PED_SCRIPT_INDEX(pid)
            local playerCoords =  GET_ENTITY_COORDS(playerPed, true)
            if !HAS_NAMED_PTFX_ASSET_LOADED("scr_xm_orbital") then REQUEST_NAMED_PTFX_ASSET("scr_xm_orbital") repeat yield() until HAS_NAMED_PTFX_ASSET_LOADED("scr_xm_orbital") end
            for i = 1, 4 do PLAY_SOUND_FROM_ENTITY(-1, "DLC_XM_Explosions_Orbital_Cannon", playerPed, 0, true, false) end
            for i = 1, 6 do ADD_OWNED_EXPLOSION(players.user_ped(), playerCoords.x, playerCoords.y, playerCoords.z + 1, 59, 1, true, false, 1.0, false) end
            USE_PARTICLE_FX_ASSET("scr_xm_orbital")
            START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_xm_orbital_blast", playerCoords.x, playerCoords.y, playerCoords.z + 1, 0, 0, 0, 1.0, true, true, true)
            yield(5, "s")
            if players.exists(pid) then OnlineRefWrapper(menu.player_root(pid), "Ares>Removal Options>Crash Options>Femboy Crash") end
        end)
    end)
    Crashes:action("Femboy Crash", {"fb", "se", "femboy"}, $"{lang.get_localised(BLOCKED)}\nMakes the victim put on thigh highs & a skirt", function()
        CrashHandler(pid, function(pid)
            util.trigger_script_event(1 << pid, {Invalid, players.user(), -2147483647, 0, 0, 0, -2147483647, -1008861746})
            yield(250)
            util.trigger_script_event(1 << pid, {Crash, players.user(), math.random(-21474, 483647), math.random(-74838, 83647), math.random(-83648, 21447), math.random(-21478, 256756), math.random(-23648, 2147), math.random(-28, 27)})
            yield(250)
            util.trigger_script_event(1 << pid, {Invalid, players.user(), -4640169, 0, 0, 0, -36565476, -53105203})
            yield(250)
            OnlineRefWrapper(menu.player_root(pid), "Crash>Elegant")
        end)
    end)
    Crashes:action("Sweet Plumbus Crash", {"admincrash", "ac"}, $"{lang.get_localised(BLOCKED)}\nGTA:V Crash admin edition!?", function()
        CrashHandler(pid, function(pid)
            local pos = players.get_position(pid)
            local getPlayerPed= GET_PLAYER_PED_SCRIPT_INDEX(pid)
            local objectModels = {
                "proc_brittlebush_01", "proc_dryplantsgrass_01", "proc_dryplantsgrass_02", 
                "proc_grasses01", "prop_dryweed_002_a", "prop_fernba", "prop_fernbb", 
                "prop_weed_001_aa", "prop_weed_002_ba", "urbandryfrnds_01", "urbangrnfrnds_01", 
                "urbangrngrass_01", "urbanweeds01", "urbanweeds01_l1", "urbanweeds02", "v_proc2_temp", 
                "prop_dandy_b", "prop_pizza_box_03", "proc_meadowmix_01", "proc_grassplantmix_02", 
                "h4_prop_bush_mang_ad", "h4_prop_bush_seagrape_low_01", "prop_saplin_002_b", 
                "proc_leafyplant_01", "prop_saplin_002_c", "proc_sml_reeds_01b", "prop_grass_dry_02", 
                "proc_sml_reeds_01c", "prop_grass_dry_03", "prop_grass_ca", "h4_prop_grass_med_01", 
                "h4_prop_bush_fern_tall_cc", "h4_prop_bush_ear_aa", "h4_prop_bush_fern_low_01", 
                "proc_lizardtail_01", "proc_drygrassfronds01", "prop_grass_da", "prop_small_bushyba", 
                "urbandrygrass_01", "proc_drygrasses01", "h4_prop_bush_ear_ab", "proc_dry_plants_01", 
                "proc_desert_sage_01", "prop_saplin_001_c", "proc_drygrasses01b", 
                "h4_prop_weed_groundcover_01", "proc_grasses01b", "prop_saplin_001_b", 
                "proc_lupins_01", "proc_grassdandelion01", "h4_prop_bush_mang_low_ab", 
                "h4_prop_grass_tropical_lush_01", "proc_indian_pbrush_01", "proc_stones_02", 
                "h4_prop_grass_wiregrass_01", "proc_sml_reeds_01", "proc_leafybush_01", 
                "h4_prop_bush_buddleia_low_01", "proc_stones_03", "proc_grassplantmix_01", 
                "h4_prop_bush_mang_low_aa", "proc_meadowpoppy_01", "prop_grass_001_a", 
                "proc_forest_ivy_01", "proc_stones_04"
            }
            local objects = {}
            for i = 1, 2 do
                for _, modelName in ipairs(objectModels) do
                    local model = util.joaat(modelName)
                    local object = entities.create_object(model, pos)
                    entities.set_can_migrate(object, false)
                    table.insert(objects, object)
                end
            end
            yield(1, "s")
            for _, object in ipairs(objects) do entities.delete_by_handle(object) end
        end)
    end)
    Crashes:action("Fragment Crash", {"fragment", "frag", "fag"}, $"{lang.get_localised(BLOCKED)}\nIncoming fragmentation grenade.", function(on_click)
        CrashHandler(pid, function(pid)
            local getPlayerPed, objects = GET_PLAYER_PED_SCRIPT_INDEX(pid), {}
            for i = 1, 5 do
                local object = entities.create_object(util.joaat("prop_fragtest_cnst_04"), GET_ENTITY_COORDS(getPlayerPed, true))
                BREAK_OBJECT_FRAGMENT_CHILD(object, 1, false)
                table.insert(objects, object)
                yield(100, "ms")
            end
            yield(1, "s")
            for _, object in ipairs(objects) do entities.delete_by_handle(object) end
        end)
    end)
end

local function setupTroll(Troll, pid)
    Troll:action("Disable Passive Mode", {""}, "Disables passive mode for the selected player.", function()
        local int = memory.read_int(memory.script_global(GlobalplayerBD_FM_3 + 1 + (pid * 610) + 512)) --  Global_1887305[PLAYER::PLAYER_ID() /*610*/].f_512
        util.trigger_script_event(1 << pid, {-366707054, players.user(), 49, 0, 0, 48, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, int})
        util.trigger_script_event(1 << pid, {1757622014, players.user(), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0})
    end)
    local PTFXSpam = Troll:list("PTFX Lags")
    local PTFX = table.freeze{
        {name="Smoke", asset = "scr_agencyheistb", particle = "scr_agency3b_elec_box"},
        {name="Clown Death", asset = "scr_rcbarry2", particle = "scr_clown_death"},
        {name="Clown Appears", asset = "scr_rcbarry2", particle = "scr_clown_appears"},
        {name="Wheel Burnout", asset = "scr_recartheft", particle = "scr_wheel_burnout"},
        {name="Orbital Blast", asset = "scr_xm_orbital", particle = "scr_xm_orbital_blast"},
        {name="Sparks Point", asset = "des_smash2", particle = "ent_ray_fbi4_sparks_point"},
        {name="Truck Slam", asset = "des_smash2", particle = "ent_ray_fbi4_truck_slam"},
        {name="Tanker", asset = "des_tanker_crash", particle = "ent_ray_tanker_exp_sp"},
        {name="Renderer Overflow (sht oil)", asset = "core", particle = "ent_sht_oil"}
    }
    for i, data in PTFX do 
        PTFXSpam:toggle_loop(data.name, {}, "", function()
            local ped = GET_PLAYER_PED_SCRIPT_INDEX(pid)
            local player_pos = GET_ENTITY_COORDS(ped, true)
            FX(data.asset)
            START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD(data.particle, player_pos.x, player_pos.y, player_pos.z, 0, 0, 0, 10.0, false, false, false)
        end, function()
            REMOVE_NAMED_PTFX_ASSET(data.asset)
        end)
    end
    Troll:action("Vehicle Kick", {"carkick"}, "Kick the player from the current vehicle.", function()
        local ped = GET_PLAYER_PED_SCRIPT_INDEX(pid)
        local vehicle = GET_VEHICLE_PED_IS_USING(ped)
        if pid == players.user() then notification.notify(scriptname, lang.get_localised(SELF)) return end
        if !IS_PED_IN_ANY_VEHICLE(ped) then notification.notify(scriptname, lang.get_localised(NOVEH):gsub("{}", players.get_name(pid))) return end
        SET_VEHICLE_EXCLUSIVE_DRIVER(vehicle, players.user_ped(), 0)
    end)
    Troll:action("Ped Spam", {}, "", function()
        local pos = players.get_position(pid)
        local spawned_peds = {}
        for i = 1, 23 do
            local ped = SpawnPed("player_one", pos, true)
            entities.set_can_migrate(ped, false)
            table.insert(spawned_peds, ped)
            yield(100)
        end
        yield(5, "s")
        for _, ped in ipairs(spawned_peds) do entities.delete_by_handle(ped) end
    end)
    Troll:action("Entity Spam", {"ohno"}, "", function()
        local pos = players.get_position(pid)
        local vehicle_models = table.freeze{
            "cargoplane", "cargoplane2", "blimp", "jet",
            "volatol", "tug", "trailerlarge", "cargobob4"
        }
        local spawned_vehicles = {}
        for i = 1, 25 do
            for _, model in ipairs(vehicle_models) do
                local vehicle = SpawnVehicle(model, pos, true)
                if vehicle then
                    entities.set_can_migrate(vehicle, false)
                    table.insert(spawned_vehicles, vehicle)
                end
                util.yield()
            end
        end
        yield(5, "s")
        for _, vehicle in ipairs(spawned_vehicles) do entities.delete_by_handle(vehicle) end
    end)
    Troll:toggle("We Do A Little Trolling", {""}, "Turns most of the trolling options on", function()
        local TrollingRefs = table.freeze{
            "Trolling>Freeze", "Trolling>Fake Money Drop","Trolling>Block Passive Mode",
            "Trolling>Force Camera Forward", "Trolling>Ragdoll", "Trolling>Send Notifications>Notification Spam",
            "Trolling>Shake Camera", "Trolling>Gravitate NPCs"
        }
        for _, Refs in ipairs(TrollingRefs) do OnlineRefWrapper(menu.player_root(pid), Refs) end
    end)
    Troll:toggle_loop("Mass Report", {""}, "This won't do anything, rockstar doesn't care", function()
        local ReportRefs = table.freeze{
            "Griefing or Disruptive Gameplay", "Cheating or Modding", "Glitching or Abusing Game Features", "Text Chat: Annoying Me",
            "Text Chat: Using Hate Speech", "Voice Chat: Annoying Me", "Voice Chat: Using Hate Speech"
        }
        for _, Report in ipairs(ReportRefs) do
            OnlineRefWrapper(menu.player_root(pid), $"Increment Commend/Report Stats>{Report}")
        end
        yield(100)
    end)
end

local function setupVehicle(Veh, pid)
    local function VehicleHandler(actionFunc)
        local car = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED_SCRIPT_INDEX(pid), false)
        if car ~= 0 then
            if entities.request_control(car, 4000) then
                actionFunc(car)
            else
                notification.notify(scriptname, "Failed to gain control of the vehicle.")
            end
        else
            notification.notify(scriptname, lang.get_localised(NOVEH):gsub("{}", players.get_name(pid)))
        end
    end
    Veh:action("Repair Vehicle", {"fixveh"}, "Repairs player's vehicle", function()
        VehicleHandler(function(vehicle)
            SET_VEHICLE_FIXED(vehicle)
        end)
    end)
    Veh:toggle_loop("Drift Mode", {""}, "Reduces their grip", function(on)
        VehicleHandler(function(vehicle)
            SET_VEHICLE_REDUCE_GRIP(vehicle, on)
        end)
    end)
    Veh:action("Detach Everything", {}, "", function()
        VehicleHandler(function(vehicle)
            local door_count = GET_NUMBER_OF_VEHICLE_DOORS(vehicle)
            POP_OUT_VEHICLE_WINDSCREEN(vehicle)
            for i = 1, door_count do SET_VEHICLE_DOOR_BROKEN(vehicle, i, false) end
            OnlineRefWrapper(menu.player_root(pid), "Trolling>Vehicle>Detach Wheel")
        end)
    end)
end

local function setupFriendly(Friendly, pid)
    local RankTo = 120
    local function FixFunc(teleportPath)
        if teleportPath then OnlineRefWrapper(menu.player_root(pid), $"Teleport>{teleportPath}") end
    end
    local loading_fixes = {"Teleport To Eclipse", "Teleport Near Me"}
    Friendly:textslider_stateful("Fix Loading", {"fix"}, "Try to fix player's infinite loading screen.", loading_fixes, function(index, value)
        if value == "Teleport To Eclipse" then
            FixFunc("Teleport To Apartment...>Eclipse Towers, Apt 31")
        elseif value == "Teleport Near Me" then
            FixFunc("Teleport To Apartment...>Near Me")
        end
    end)
end

local function setupTeleport(Teleports, pid)
    local function ForceTP(event, coords)
        if players.is_marked_as_modder(pid) then
            return notification.notify(scriptname, $"Failed to teleport {players.get_name(pid)}\nReason: Marked as Modder")
        end
        local executeAction = function()
            if event then
                util.trigger_script_event(1 << pid, event)
            elseif coords then
                players.teleport_3d(pid, table.unpack(coords))
                yield(1, "s")
                players.teleport_3d(pid, table.unpack(coords))
            end
        end
        if event then
            executeAction()
        end
    end
    local teleportLocations = table.freeze{
        {name = "Timeout", coords = {25.617956, 7645.24, 18.880297}}, {name = "Beach", coords = {-1476.292, -1456.7183, 2.1505792}},
        {name = "Airport", coords = {-1233.7285, -3109.5854, 13.956394}}, {name = "AFK", coords = {-150.9991, -967.3635, 149.13132}},
        {name = "Bank Vault", coords = {263.39627, 214.39891, 101.68336}}, {name = "Dark Room", coords = {-1922.0615, 3749.7983, -99.64585}},
        {name = "High Up", coords = {119.62905, -621.9366, 2696.0469}}, {name = "Side of the map", coords = {7750.895, -15998.681, 401.48337}},
    }
    local missionEndTeleports = table.freeze{
        [4] = "Sandy Shores", [7] = "Tequi-La-La", [8] = "LSIA (Bottom Level)", [9] = "Yellow Jack Bar", [10] = "Spitroasters Meat House", 
        [11] = "Up-n-Atom Burger", [13] = "Alamo Fruit Market", [25] = "Lester's Warehouse", [28] = "Benny's Shop", 
        [31] = "Sandy Shores Boat House", [42] = "Hookies Food Diner", [56] = "Paleto Bay", [58] = "Grapeseed Airfield", 
        [59] = "Paleto Bay Ammunation", [60] = "LSIA (Top Level)", [66] = "Observatory", [68] = "Casino", [72] = "Casino Roof", 
        [87] = "Martin Madrazo's House", [90] = "LS Docks", [91] = "Del Perro Pier", [97] = "Country Club", [114] = "Mount Chiliad"
    }
    local interiorTpList = table.freeze{
        [70] = "Bunker", [81] = "Mobile Operations Center", [83] = "Hangar", [88] = "Avenger", [89] = "Facility", 
        [102] = "Nightclub Garage", [117] = "Terrorbyte", [122] = "Arena Workshop", [123] = "Casino", 
        [124] = "Penthouse", [128] = "Arcade Garage", [146] = "Nightclub", [147] = "Kosatka", [149] = "Auto Shop", 
        [155] = "Agency Office", [159] = "Acid Lab", [160] = "Freakshop", [161] = "Eclipse Blvd Garage", [164] = "Scrapyard"
    }
    local freemodeMissionWarps = table.freeze{
        [267] = "Junk Energy Skydive", [292] = "Bike Service", [296] = "Ammunation Contract", [304] = "Acid Lab Setup",
        [308] = "Stash House Mission", [309] = "Taxi Mission", [318] = "Time Trial", [324] = "Tow Truck Service"
    }
    local customTPs = Teleports:list("Preset Teleports", {""}, lang.get_localised(BLOCKED))
    for _, location in ipairs(teleportLocations) do
        customTPs:action(location.name, {""}, "", function()
            ForceTP(nil, location.coords, false)
        end)
    end
    local missionWarp = Teleports:list("Freemode Mission Warp", {""}, lang.get_localised(BLOCKED))
    for missionType, warpName in pairs(freemodeMissionWarps) do
        missionWarp:action(warpName, {""}, "", function()
            ForceTP({Challenge, players.user(), missionType, -1}, nil)
        end)
    end
    local interiorTP = Teleports:list("Interior Teleports", {""}, lang.get_localised(BLOCKED))
    for id, interior in pairs(interiorTpList) do
        interiorTP:action(interior, {""}, "", function()
            ForceTP({WarpInside, players.user(), id, 1, 0, 1, 1130429716, -1001012850, 1106067788, 0, 0, 1, 2123789977, 1, -1}, nil, true)
        end)
    end
    local missionEndTP = Teleports:list("Mission End Teleports", {""}, lang.get_localised(BLOCKED))
    for id, missionWarp in pairs(missionEndTeleports) do
        missionEndTP:action(missionWarp, {""}, "", function()
            local handle = NETWORK_HASH_FROM_PLAYER_HANDLE(pid)
            ForceTP({LaunchSync, players.user(), id, 4, handle, 0, 0, 0, 1, 1}, nil)
        end)
    end
end

local function setupDetection(Detections, pid)
    local name = Detections:text_input("Detection Name", {"detectionname"}, "", function()
    end)
    local danger = 1
    local severity = Detections:slider("Severity", {"Severitylvl"}, "0 = No classification\n50 = Likely Modder\n100 = Modder", 0, 100, 0, 50, function(danger2)
        danger = danger2
    end)
    
    Detections:action("Add Detection", {}, "", function()
        players.add_detection(pid, $"{name.value}", TOAST_ALL, $"{severity.value}")
    end)
end

local function setupHidden(hidden, pid)
    local function DesyncKick(pid)
        if players.get_host() == pid then notification.notify(scriptname, lang.get_localised(ONHOST)) return end
        if !Host_Check() then
            OnlineRefWrapper(menu.player_root(pid), "Kick>Love Letter")
        else
            notification.notify(scriptname, "Give this around 30-40 seconds to kick them")
            OnlineRefWrapper(menu.player_root(pid), "Kick>Love Letter")
            yield(10, "s")
            if players.exists(pid) then OnlineRefWrapper(menu.player_root(pid), "Kick>Orgasm") end
        end
    end
    OnlineHidden = table.freeze{
        {"Smart Desync Kick", {"llk", "ll", "lk"}, "Desync kicks the player, Changes depending if you're host or not\nHas a chance to backfire if not host", function() DesyncKick(pid) end},
        {"Host Kick", {"hk", "host"}, "", function() if !Host_Check() then OnlineRefWrapper(menu.player_root(pid), "Kick>Host") else notification.notify(scriptname, lang.get_localised(HOST)) return end end},
        {"Steamroller", {"sr", "steam", "roll"}, "", function() OnlineRefWrapper(menu.player_root(pid), "Crash>Steamroller") end},
    }
    for _, HideOnlineRoot in ipairs(OnlineHidden) do
        local cmds = hidden:action(table.unpack(HideOnlineRoot))
        menu.set_visible(cmds, false)
    end
end

if developer() and filesystem.exists($"{filesystem.store_dir()}\\AresBeta\\__Dev\\DevOnline.lua") then
    local status, err = pcall(function() require("Store.AresBeta.__Dev.DevOnline") end)
    if !status then util.toast($"[Ares] Error loading DevOnline module\n{err}", TOAST_DEFAULT | TOAST_LOGGER) end
end
local function JoinHandler(pid)
    menu.player_root(pid):divider("Ares")
    local oAres = menu.player_root(pid):list("Ares", {"Ares"})
    local menuItems = {
        {name = "Friendly", desc = "", setupFunc = setupFriendly, hidden = false},
        {name = "Vehicle", desc = "", setupFunc = setupVehicle, hidden = false},
        {name = "Trolling", desc = "", setupFunc = setupTroll, hidden = false},
        {name = "Teleport", desc = "", setupFunc = setupTeleport, hidden = false},
        {name = "Detection", desc = "", setupFunc = setupDetection, hidden = false},
        {name = "Removal", desc = "", setupFunc = setupRemoval, hidden = false},
        {name = "Kick", parent = "Removal", desc = "", setupFunc = setupKick, hidden = false},
        {name = "Blacklist", parent = "Removal", desc = "", setupFunc = setupBlacklist, hidden = false},
        {name = "Crash", parent = "Removal", desc = "", setupFunc = setupCrash, hidden = false},
        {name = "Hidden", desc = "This shouldn't be showing", setupFunc = setupHidden, hidden = true},
        {name = "Developer", desc = "", setupFunc = setupDev, hidden = !Dev}
    }
    local menus, parents = {}, {}
    for _, item in ipairs(menuItems) do
        local success, result = pcall(function()
            if item.name == "Removal" then
                menus[item.name] = oAres:list(item.name, {}, item.desc)
                if item.setupFunc then item.setupFunc(menus[item.name], pid) end
                parents[item.name] = menus[item.name]
            elseif item.parent then
                if !parents[item.parent] then util.toast($"[Ares] Caught an Exception (\"{item.parent}\")") end
                menus[item.name] = parents[item.parent]:list(item.name, {}, item.desc)
                if item.setupFunc then item.setupFunc(menus[item.name], pid) end
            else
                menus[item.name] = oAres:list(item.name, {}, item.desc)
                if item.setupFunc then item.setupFunc(menus[item.name], pid) end
            end
            if item.hidden then menu.set_visible(menus[item.name], false) end
        end)
        if !success then
            util.toast("[Ares] Caught an Exception (P2)", TOAST_DEFAULT | TOAST_CONSOLE)
            if DebugModules then util.log($"[Ares] {item.name}: {result}") end
        end
    end
end

players.on_join(JoinHandler)
players.dispatch_on_join()