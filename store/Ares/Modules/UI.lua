local function VehicleOverlay(VehUI)
    local veh = entities.get_user_vehicle_as_handle(false)
    local showElements = false
    local displayInMPH = false

    local colours = {
        engineHealth = {r = 160/255, g = 0/255, b = 0/255, a = 1},
        bodyHealth = {r = 200/255, g = 0/255, b = 0/255, a = 1},
        speed = {r = 200/255, g = 0/255, b = 0/255, a = 1},
        countermeasures = {r = 200/255, g = 0/255, b = 0/255, a = 1},
        text = {r = 255/255, g = 255/255, b = 255/255, a = 255/255},
    }
    
    local function drawTextWithShadow(x, y, text, alignment, scale, textColor, shadowColor)
        shadowColor = shadowColor or { r=0, g=0, b=0, a=1 }
        local offsetX = 0.0005
        local offsetY = -0.0005
        local shadowX = x + offsetX
        local shadowY = y + offsetY
        directx.draw_text_client(shadowX, shadowY, text, alignment, scale, shadowColor)
        directx.draw_text_client(x, y, text, alignment, scale, textColor)
    end
    
    local function drawShapes(x, y, width, height, Percentage, text, color)
        local barWidth = width * Percentage
        local pointX = x + barWidth
        local pointY2 = y + height
        local trix = x + width
        local colorBack = {r = 41/255, g = 39/255, b = 40/255, a = 1}
        local textColor = {r = 255/255, g = 255/255, b = 255/255, a = 255/255}
        local textScale = 0.5
        if !string.find(text, "Body") then directx.draw_rect(x, y, width, height, colorBack) end
        local _, textHeight = directx.get_text_size(text, textScale) 
        if string.find(text, "Body") then
            drawTextWithShadow(pointX, y + height + textHeight, text, ALIGN_CENTRE, textScale, textColor)
        else
            drawTextWithShadow(pointX, y - textHeight, text, ALIGN_CENTRE, textScale, textColor)
        end
        directx.draw_rect(x, y, barWidth, height, color)
        directx.draw_triangle(pointX, y, pointX, pointY2, barWidth + 0.8032, y + height / 2, color) --pointX needs to be changed in order for the pointy side to work or move?
    end
    
    local function round(value)
        local roundedValue = string.format("%.1f", value)
        return roundedValue
    end
    
    local function getEngineHealthPercentage()
        local minHealth = -4000
        local maxHealth = 1000
        local currentHealth = GET_VEHICLE_ENGINE_HEALTH(veh)
        return math.min(1, math.max(0, (currentHealth - minHealth) / (maxHealth - minHealth))), round(currentHealth)
    end
    
    local function getBodyHealthPercentage()
        local maxHealth = 1000
        local currentHealth = GET_VEHICLE_BODY_HEALTH(veh)
        return math.min(1, math.max(0, currentHealth / maxHealth)), round(currentHealth)
    end
    
    local function getSpeed()
        local conversionFactor = displayInMPH and 2.237 or 3.6
        local speedUnit = displayInMPH and "MPH" or "KM/H"
        local maxSpeed = (GET_VEHICLE_ESTIMATED_MAX_SPEED(veh) * conversionFactor) * 1.5
        local currentSpeed = GET_ENTITY_SPEED(veh) * conversionFactor
        return math.min(1, math.max(0, currentSpeed / maxSpeed)), round(currentSpeed), speedUnit
    end
    
    local function getCounterMeasures()
        local currentCounter = GET_VEHICLE_COUNTERMEASURE_AMMO(veh)
        local Ctype = "NaN"
        local maxCounter
        local mdl = entities.get_model_hash(veh)
        if DECOR_GET_INT(veh, "Player_Vehicle") ~= 0 and !IS_THIS_MODEL_A_CAR(mdl) or mdl == util.joaat("oppressor2") then
            if entities.get_upgrade_value(veh, 1) == 0 or entities.get_upgrade_value(veh, 6) == 0 then
                Ctype = "Chaff"
                maxCounter = 10
            elseif entities.get_upgrade_value(veh, 1) == 1 or entities.get_upgrade_value(veh, 6) == 1 then
                Ctype = "Flares"
                maxCounter = 20
            end
        else
            return false
        end
        if Ctype ~= "NaN" then
            return math.min(1, math.max(0, currentCounter / maxCounter)), currentCounter, Ctype
        else
            return false
        end
    end
    
    local function drawElements()
        local barWidth = 0.1
        local barHeight = 0.015
        local paddingX = 0.1
        local y = 0.8
        local engineBarX = 1.0 - barWidth - paddingX
        local SpeedBarX = 1.0 - barWidth - paddingX
        local engineHealthPercentage, engineCurrentHealth = getEngineHealthPercentage()
        local bodyHealthPercentage, bodyCurrentHealth = getBodyHealthPercentage()
        local speedPercentage, currentSpeed, speedUnit = getSpeed()
        local counterPercentage, counterCurrent, Ctype = getCounterMeasures()
        local vehmdl = util.get_label_text(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(players.get_vehicle_model(players.user())))
        drawShapes(engineBarX, y, barWidth, barHeight, engineHealthPercentage, $"Engine: {engineCurrentHealth}", colours.engineHealth)
        drawShapes(engineBarX, y, barWidth, barHeight, bodyHealthPercentage, $"Body: {bodyCurrentHealth}", colours.bodyHealth)
        drawShapes(SpeedBarX, y - 0.06, barWidth, barHeight + 0.01, speedPercentage,$"{currentSpeed} {speedUnit}", colours.speed)
        drawTextWithShadow(0.9, y - 0.09, vehmdl, ALIGN_BOTTOM_RIGHT, 0.8, colours.text)
        if counterPercentage then
            drawShapes(SpeedBarX, y - 0.14, barWidth, barHeight - 0.01, counterPercentage, $"{counterCurrent} {Ctype}", colours.countermeasures)
        end
    end
    
    util.create_tick_handler(function()
        if showElements and IS_PED_IN_ANY_VEHICLE(players.user_ped(), false) and !util.is_session_transition_active() then
            veh = entities.get_user_vehicle_as_handle(false)
            drawElements()
        end
    end)
    
    local function toggleElements(toggled) showElements = toggled end
    VehUI:toggle("Show Info Display", {}, "Toggle the display of vehicle elements.", toggleElements)
    VehUI:toggle("MPH", {""}, "Converts KM/H to MPH", function(toggled) displayInMPH = toggled end)

    local engineHealthRoot = VehUI:list("Engine Health Colour", {}, "Set the color for engine health.")
    engineHealthRoot:colour("Engine Health Colour", {"enginehealthcolour"}, "", colours.engineHealth, true, function(colour)
        colours.engineHealth = colour
    end):rainbow()
    
    local bodyHealthRoot = VehUI:list("Body Health Colour", {}, "Set the color for body health.")
    bodyHealthRoot:colour("Body Health Colour", {"bodyhealthcolour"}, "", colours.bodyHealth, true, function(colour)
        colours.bodyHealth = colour
    end):rainbow()
    
    local speedRoot = VehUI:list("Speed Colour", {}, "Set the color for speed.")
    speedRoot:colour("Speed Colour", {"speedcolour"}, "", colours.speed, true, function(colour)
        colours.speed = colour
    end):rainbow()
    
    local countermeasuresRoot = VehUI:list("Countermeasures Colour", {}, "Set the color for countermeasures.")
    countermeasuresRoot:colour("Countermeasures Colour", {"countermeasurescolour"}, "", colours.countermeasures, true, function(colour)
        colours.countermeasures = colour
    end):rainbow()
    
    local textRoot = VehUI:list("Text Colour", {}, "Set the color for text.")
    textRoot:colour("Text Colour", {"textcolour"}, "", colours.text, true, function(colour)
        colours.text = colour
    end):rainbow()
end

local function InfoOverlay(InfoUi)
end

local function RegisterUI()
    local UI = menu.my_root():list("Hud", {}, "")
    local HudItems = {
        {name = "Vehicle Overlay", setupFunc = VehicleOverlay, hidden = false},
    }
    for _, item in ipairs(HudItems) do
        local success, result = pcall(function()
            local menuList = UI:list(item.name, {}, item.desc or "")
            if item.setupFunc then item.setupFunc(menuList) end
            if item.hidden then menu.set_visible(menuList, false) end
        end)
        if !success then
            util.toast("[Ares] Caught an Exception during UI setup", TOAST_DEFAULT | TOAST_CONSOLE)
            if DebugModules then util.log($"[Ares] {item.name}: {result}") end
        end
    end
end

RegisterUI()