local function Root(Self)
    local Snacks = {
        {"NO_BOUGHT_HEALTH_SNACKS", 10}, {"NO_BOUGHT_EPIC_SNACKS", 10}, {"NUMBER_OF_ORANGE_BOUGHT", 10},
        {"NUMBER_OF_BOURGE_BOUGHT", 10}, {"NUMBER_OF_CHAMP_BOUGHT", 10}, {"NUMBER_OF_SPRUNK_BOUGHT", 10},
        {"CIGARETTES_BOUGHT", 10}, {"NO_BOUGHT_YUM_SNACKS", 10}
    }

    Self:toggle_loop("Auto Restock", {""}, "Fully stocked snacks and armor in your inventory at all times", function()
        for i = 1, 5 do Snacks[#Snacks + 1] = {$"MP_CHAR_ARMOUR_{i}_COUNT", 10} end
        for i, stat in ipairs(Snacks) do SET_STAT(stat[1], stat[2]) end
    end)

    Self:toggle_loop("True No Ragdoll", {""}, "Speeds up getting up after being knocked down", function()
        SET_PED_CONFIG_FLAG(players.user_ped(), 227, IS_PLAYER_PLAYING(players.user()))
    end)

    Self:toggle_loop("Instant Respawn", {""}, "Instantly respawns you, skipping the wasted screen & respawn", function()
        if InSession() and IS_ENTITY_DEAD(players.user_ped()) then
            local pos = GET_ENTITY_COORDS(players.user_ped(), true)
            local heading = GET_ENTITY_HEADING(players.user_ped())
            NETWORK_RESURRECT_LOCAL_PLAYER(pos.x, pos.y, pos.z, heading, true, false, false, 0, 0)
            yield(100, "ms")
        end
    end)

    Self:toggle("Tiny Player", {""}, "", function(on) SET_PED_CONFIG_FLAG(players.user_ped(), 223, on) end)
end

local function HolsterWeapon(Self)
    local function request_weapon(...)
        local models = {...}
        for _, model in ipairs(models) do
            if !HAS_WEAPON_ASSET_LOADED(model) then
                REQUEST_WEAPON_ASSET(model, 31, 26)
            end
        end
        while true do
            local allLoaded = true
            for _, model in ipairs(models) do
                if !HAS_WEAPON_ASSET_LOADED(model) then
                    allLoaded = false
                    break
                end
            end
            if allLoaded then break end
            yield()
        end
    end

    local function attachWeapon(spawnweapon, boneIndex, x, y, z, rx, ry, rz)
        ATTACH_ENTITY_TO_ENTITY(spawnweapon, PLAYER_PED_ID(), boneIndex, x, y, z, rx, ry, rz, false, true, false, false, 2, true, 0)
    end

    local function AttachWeapons(spawnweapon, weaponHash)
        local weaponGroup = GET_WEAPONTYPE_GROUP(weaponHash)
        if GET_WEAPONTYPE_GROUP(weaponHash) == 416676503 or GET_WEAPONTYPE_GROUP(weaponHash) == 690389602 then 
            attachWeapon(spawnweapon, GET_PED_BONE_INDEX(PLAYER_PED_ID(), 0x192A), 0.01, 0, 0.15, 70, 0.50, 0)
        elseif weaponHash == util.joaat("weapon_bat") then
            attachWeapon(spawnweapon, GET_PED_BONE_INDEX(PLAYER_PED_ID(), 0x60F2), 0.3, -0.18, -0.15, 0, 300, 0)
        elseif table.contains(weaponHashes.melee, weaponHash) then
            attachWeapon(spawnweapon, GET_PED_BONE_INDEX(PLAYER_PED_ID(), 0x60F2), 0.2, -0.18, -0.1, 0, 300, 0)
        elseif table.contains(weaponHashes.hammer, weaponHash) then
            attachWeapon(spawnweapon, GET_PED_BONE_INDEX(PLAYER_PED_ID(), 0x192A), 0.2, 0, 0.13, 0, 270, 270)
        elseif table.contains(weaponHashes.knife, weaponHash) then
            attachWeapon(spawnweapon, GET_PED_BONE_INDEX(PLAYER_PED_ID(), 0x192A), 0.2, 0, 0.13, 0, -270, 270)
        elseif table.contains(weaponHashes.flashlight, weaponHash) then
            attachWeapon(spawnweapon, GET_PED_BONE_INDEX(PLAYER_PED_ID(), 0x192A), 0, 0, 0.15, 0, -270, 270)
        elseif table.contains(weaponHashes.petrolcan, weaponHash) then
            attachWeapon(spawnweapon, GET_PED_BONE_INDEX(PLAYER_PED_ID(), 0x60F2), 0, -0.18, 0, 0, 90, 0)
        elseif table.contains(weaponHashes.explosives, weaponHash) then
            attachWeapon(spawnweapon, GET_PED_BONE_INDEX(PLAYER_PED_ID(), 0x192A), 0.2, 0, 0.13, 0, 0, 270)
        elseif weaponHash == util.joaat("weapon_fireextinguisher") then
            attachWeapon(spawnweapon, GET_PED_BONE_INDEX(PLAYER_PED_ID(), 0x192A), 0, -0.05, 0.13, 0, 270, 90)
        else
            attachWeapon(spawnweapon, GET_PED_BONE_INDEX(PLAYER_PED_ID(), 0x60F2), 0, -0.18, 0, 180, -220, 0)
        end
    end

    local weaponHashes = {
        melee = {
            util.joaat("weapon_bat"), util.joaat("weapon_crowbar"), util.joaat("weapon_battleaxe"),
            util.joaat("weapon_golfclub"), util.joaat("weapon_hatchet"), util.joaat("weapon_poolcue"),
            util.joaat("weapon_stone_hatchet"), util.joaat("weapon_knuckle")
        },
        hammer = {
            util.joaat("weapon_hammer"), util.joaat("weapon_candycane"), util.joaat("weapon_wrench")
        },
        knife = {
            util.joaat("weapon_bottle"), util.joaat("weapon_dagger"), util.joaat("weapon_knife"),
            util.joaat("weapon_machete"), util.joaat("weapon_switchblade")
        },
        flashlight = {
            util.joaat("weapon_flashlight"), util.joaat("weapon_nightstick")
        },
        petrolcan = {
            util.joaat("weapon_petrolcan"), util.joaat("weapon_hazardcan"), util.joaat("weapon_fertilizercan")
        },
        explosives = {
            util.joaat("weapon_proxmine"), util.joaat("weapon_stickybomb"), util.joaat("weapon_ball"),
            util.joaat("weapon_bzgas"), util.joaat("weapon_flare"), util.joaat("weapon_molotov"),
            util.joaat("weapon_pipebomb"), util.joaat("weapon_smokegrenade"), util.joaat("weapon_snowball")
        }
    }

    local weaponback, curweap, spawnweapon = false, -1569615261, 0
    Self:toggle("Holster Weapon", {""}, "Puts the weapon you're holding on your back", function(on)
        weaponback = on
        spawnweapon = on and 0 or spawnweapon
        while weaponback do
            local selectedWeaponHash = HUD_GET_WEAPON_WHEEL_CURRENTLY_HIGHLIGHTED(PLAYER_PED_ID())
            if selectedWeaponHash ~= curweap and selectedWeaponHash ~= -1569615261 then
                if spawnweapon ~= 0 then entities.delete_by_handle(spawnweapon) end
                curweap = selectedWeaponHash
                local pos = GET_ENTITY_COORDS(PLAYER_PED_ID(), true)
                request_weapon(curweap)
                spawnweapon = CREATE_WEAPON_OBJECT(curweap, 1, pos.x, pos.y, pos.z, true, 1.0, 0, 0, 0)
                AttachWeapons(spawnweapon, curweap)
            end
            yield()
        end
        if spawnweapon ~= 0 then 
            entities.delete_by_handle(spawnweapon) 
        end
    end)
end

local function Ragdoll(ragdoll)
    local ragdollKey = 0x51
    local ragdollType = 0
    local playerIsRagdoll = false

    ragdoll:toggle_loop("Quick Stand", {}, "Allows you to get up faster when knocked down.", function()
        SET_PED_CONFIG_FLAG(players.user_ped(), 227, IS_PLAYER_PLAYING(players.user()))
    end, 
    function() 
        SET_PED_CONFIG_FLAG(players.user_ped(), 227, false)
    end)

    ragdoll:toggle("Clumsiness [Shortcut]", {}, "Toggles the 'Clumsiness' option in Stand's Self tab.", function(toggle)
        menu.ref_by_path("Self>Clumsiness").value = toggle
    end)

    ragdoll:toggle_loop("Stay Down", {}, "Prevents you from getting back up after being ragdolled.", function()
        if menu.ref_by_path("Self>Gracefulness").value then
            util.toast("Don't enable Gracefulness and Stay Down simultaneously.")
            menu.ref_by_path($"Stand>{SCRIPT_NAME}>Self>Ragdoll Options>Stay Down", 55).value = false
        end
        if IS_PED_RAGDOLL(players.user_ped()) then playerIsRagdoll = true end
        if playerIsRagdoll then SET_PED_TO_RAGDOLL(players.user_ped(), 750, 750, 0, false, false, false) end
    end, 
    function() 
        playerIsRagdoll = false 
    end)

    ragdoll:divider("Ragdoll Toggle")

    ragdoll:toggle_loop("Ragdoll Toggle", {"ragdolltoggle"}, "Hold the specified key to ragdoll.", function()
        if menu.ref_by_path("Self>Gracefulness").value then
            util.toast("Pro Tip: Don't enable Gracefulness and Ragdoll Toggle simultaneously. ;)")
            menu.ref_by_path($"Stand>{SCRIPT_NAME}>Self>Ragdoll Options>Ragdoll Toggle", 55).value = false
        end
        if !menu.command_box_is_open() and !IS_MP_TEXT_CHAT_TYPING() and 
           !IS_SYSTEM_UI_BEING_DISPLAYED() and util.is_key_down(ragdollKey) then
            SET_PED_TO_RAGDOLL(players.user_ped(), 750, 750, ragdollType, false, false, false)
        end
        menu.ref_by_path("Game>Disables>Disable Game Inputs>DUCK", 55).value = (ragdollKey == 0xA2)
    end, 
    function() 
        menu.ref_by_path("Game>Disables>Disable Game Inputs>DUCK", 55).value = false 
    end)

    ragdoll:list_select("Ragdoll Toggle Key", {}, "Change the key used for Ragdoll Toggle.", {{0x51, "Q"}, {0x45, "E"}, {0x52, "R"}, {0x58, "X"}, {0xA2, "Left Control"}}, 0x51, function(value)
        ragdollKey = value
    end)

    ragdoll:list_select("Ragdoll Type", {}, "Change the type of ragdoll used for Ragdoll Toggle.", {{0, "Normal"}, {2, "Narrow leg stumble"}, {3, "Wide leg stumble"}}, 0, function(value)
        ragdollType = value
    end)
end

local function PaintGun(Paint)
    local Decals = table.freeze{
        [1030] = "splatters_paint", [1050] = "splatters_water_hydrant",
        [4010] = "weapImpact_metal", [4020] = "weapImpact_concrete",
        [4030] = "weapImpact_mattress", [4032] = "weapImpact_mud",
        [4050] = "weapImpact_wood", [4053] = "weapImpact_sand",
        [4040] = "weapImpact_cardboard", [4100] = "weapImpact_melee_glass",
        [4102] = "weapImpact_glass_blood", [4104] = "weapImpact_glass_blood2",
        [4200] = "weapImpact_shotgun_paper", [4310] = "weapImpact_melee_concrete",
        [4312] = "weapImpact_melee_wood", [4314] = "weapImpact_melee_metal",
        [5000] = "bang_concrete_bang", [5004] = "bang_bullet_bang2", 
        [5031] = "bang_glass", [9000] = "solidPool_water", [9050] = "liquidTrail_water"
    }

    local paintColour = {r = 255, g = 0, b = 255}
    local paintScale = 0.5
    local coordsPTG = memory.alloc()
    local paintDecal = 1030

    Paint:list_select("Paint Type", {}, "", Decals, 1030, function(value)
        paintDecal = value
    end)

    Paint:toggle_loop("Paint Gun", {"paintgun"}, "Shoots paint instead of bullets.", function()
        SET_DECAL_BULLET_IMPACT_RANGE_SCALE(0.0)
        if GET_PED_LAST_WEAPON_IMPACT_COORD(players.user_ped(), coordsPTG) then
            local coords = v3.new(memory.read_vector3(coordsPTG))
            if coords.z ~= 0.0 then
                ADD_DECAL(paintDecal, coords.x, coords.y, coords.z, 0, 0, -1, 0, 1.0, 0.0, paintScale, paintScale - 0.1, 
                    paintColour.r / 255, paintColour.g / 255, paintColour.b / 255, 1, -1, true, false, false)
                REMOVE_PARTICLE_FX_IN_RANGE(coords.x, coords.y, coords.z, 1.0)
            end
        end
    end, function()
        SET_DECAL_BULLET_IMPACT_RANGE_SCALE(1.0)
    end)

    Paint:action("Clear Paint", {"clearpaint"}, "Clear All Paint.", function()
        REMOVE_DECALS_IN_RANGE(0.0, 0.0, 0.0, 10000.0)
    end)

    Paint:colour("Paint Colour", {"paintcolour"}, "Change the colour of the paint.", paintColour.r / 255, paintColour.g / 255, paintColour.b / 255, 1.0, false, function(colour)
        paintColour = {r = colour.r * 255, g = colour.g * 255, b = colour.b * 255}
    end):rainbow()

    Paint:slider_float("Paint Scale", {"paintscale"}, "Adjust the size of the paint splatter.", 5, 1000, paintScale * 10, 1, function(value)
        paintScale = value / 10.0
    end)
end

local function QuickAnimations(Anim)
    local taskList = {
        {160, "TASK_ENTER_VEHICLE", "Quickly enter vehicles"}, {2, "TASK_EXIT_VEHICLE", "Quickly exit vehicles"},
        {121, "TASK_STEAL_VEHICLE", "Quickly steal vehicles"}, {152, "TASK_LEAVE_ANY_CAR", "Quickly leave vehicles"},
        {56, "TASK_SWAP_WEAPON", "Quickly change weapons"}, {1, "TASK_CLIMB_LADDER", "Quickly climb ladders"},
        {298, "TASK_RELOAD_GUN", "Quickly reload weapons"}, {50, "TASK_VAULT", "Quickly climb"},
        {51, "TASK_DROP_DOWN", "Quickly drop down"}, {128, "TASK_MELEE", "Quickly Melee"}
    }
    for _, task in ipairs(taskList) do
        local taskId, taskName, taskDescription = table.unpack(task)
        Anim:toggle_loop(taskName, {""}, taskDescription, function()
            if GET_IS_TASK_ACTIVE(players.user_ped(), taskId) then
                FORCE_PED_AI_AND_ANIMATION_UPDATE(players.user_ped())
            end
        end)
    end
end

local function RegisterSelf()
    local Self = menu.my_root():list("Self", {}, "")
    local SelfItems = {
        {name = "Paint Gun", setupFunc = PaintGun, hidden = false},
        {name = "Ragdoll", setupFunc= Ragdoll, hidden= false},
        {name = "Quick Animations", setupFunc = QuickAnimations, hidden = false},
    }
    HolsterWeapon(Self)
    Root(Self)
    for _, item in ipairs(SelfItems) do
        local success, result = pcall(function()
            local menuList = Self:list(item.name, {}, item.desc or "")
            if item.setupFunc then item.setupFunc(menuList) end
            if item.hidden then menu.set_visible(menuList, false) end
        end)
        if !success then
            util.toast("[Ares] Caught an Exception during Self setup", TOAST_DEFAULT | TOAST_CONSOLE)
            if DebugModules then util.log($"[Ares] {item.name}: {result}") end
        end
    end
end

RegisterSelf()