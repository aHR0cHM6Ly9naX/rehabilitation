local function registerExtras()
    local AresUtils = menu.attach_before(menu.ref_by_path("Game>Info Overlay>In-Game Time"), menu.list(menu.shadow_root(), SCRIPT_NAME.." Utils", {}))
    
    local function createToggleOption(title, keys, displayTextFunc)
        return AresUtils:toggle_loop(title, keys, "", function()
            util.draw_debug_text(displayTextFunc())
        end)
    end

    local function countPlayers(conditionFunc)
        local count = 0
        for _, pid in pairs(players.list()) do 
            if players.exists(pid) and conditionFunc(pid) then
                count = count + 1
            end
        end
        return count
    end
    
    createToggleOption("OS Time", {""}, function() return os.date("%I:%M %p") end)
    createToggleOption("OS Date", {""}, function() return os.date("%a, %d. %B") end)

    createToggleOption("Organization Slots", {""}, function() 
        local orgCount = countPlayers(function(pid)
            return players.get_boss(pid) == pid and pid ~= -1
        end)
        return $"${orgCount}/10 Organisations"
    end)

    createToggleOption("Controller", {""}, function() 
        local controllerCount = countPlayers(function(pid)
            return players.is_using_controller(pid) and pid ~= -1
        end)
        return $"${controllerCount} Controller"
    end)

    createToggleOption("Keyboard", {""}, function() 
        local keyboardCount = countPlayers(function(pid)
            return not players.is_using_controller(pid) and pid ~= -1
        end)
        return $"${keyboardCount} Keyboard"
    end)
end

registerExtras()