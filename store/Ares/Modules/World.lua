local function Root(World)
	World:toggle_loop("Enable Snow", {""}, "Forces snow on the map\nThis will only affect your game", function()
        _FORCE_GROUND_SNOW_PASS(true)
    end,function()
        _FORCE_GROUND_SNOW_PASS(false)
    end)
    
    World:toggle_loop("Part Water", {""}, "Makes you like Moses\nThis will only affect your game", function()
        local coords = GET_ENTITY_COORDS(PLAYER_PED_ID(), true)
        MODIFY_WATER(coords.x, coords.y, -25000000.0, 0.2)
    end)
end

local function WaterManipulation(WM)
    --WATER_OVERRIDE_SET_SHOREWAVEAMPLITUDE

    --WATER_OVERRIDE_SET_SHOREWAVEMINAMPLITUDE

    --WATER_OVERRIDE_SET_SHOREWAVEMAXAMPLITUDE

    --WATER_OVERRIDE_SET_OCEANNOISEMINAMPLITUDE

    --WATER_OVERRIDE_SET_OCEANWAVEAMPLITUDE

    --WATER_OVERRIDE_SET_OCEANWAVEMINAMPLITUDE

    --WATER_OVERRIDE_SET_OCEANWAVEMAXAMPLITUDE

    --WATER_OVERRIDE_SET_RIPPLEBUMPINESS

    --WATER_OVERRIDE_SET_RIPPLEMINBUMPINESS

    --WATER_OVERRIDE_SET_RIPPLEMAXBUMPINESS

    --WATER_OVERRIDE_SET_RIPPLEDISTURB
end

local function ManageTraffic(Traffic)
	local pop_multiplier_id
    Traffic:toggle_loop("No Traffic (Lobby)", {"notrafficlobby"}, "Disables traffic throughout the lobby", function()
        CLEAR_AREA_OF_VEHICLES(1.1, 1.1, 1.1, 19999.9, false, false, false, false, true, false, 0)
        CLEAR_AREA_OF_PEDS(1.1, 1.1, 1.1, 19999.9, 1)
        util.yield_once()
    end)
    
    Traffic:toggle_loop("No Traffic (Local)", {"notrafficlocal"}, "Disables all traffic near you", function()
        local pos = players.get_position(players.user())
        CLEAR_AREA_OF_VEHICLES(pos.x, pos.y, pos.z, 200, false, false, false, false, true, false, 0)
        CLEAR_AREA_OF_PEDS(pos.x, pos.y, pos.z, 500, 1)
        util.yield_once()
    end)
    
    Traffic:toggle_loop("Delete Modded Pop Multiplier Areas", {""}, "Deletes modded population multiplier areas that stand misses\n(Triggers Stand's no mod pop as well)", function()
        protectionRef = menu.ref_by_path("Online>Protections>Delete Modded Pop Multiplier Areas")
        if !protectionRef.value then protectionRef.value = true end
            for i = -1, 100 do
                if DOES_POP_MULTIPLIER_AREA_EXIST(i) then
                    if IS_POP_MULTIPLIER_AREA_NETWORKED(i) then
                        util.toast($"Found a Modded Pop Multiplier Area with ID: {i}...\nRemoving them...")
                    end
                    REMOVE_POP_MULTIPLIER_AREA(i, true)
                end
            end
        end, function()
        protectionRef.value = false
    end)
end

local function CleanUp(Cleanse)
	Cleanse:action("Super cleanse", {"supercleanse", "sclean", "cleanse"}, "Delete all entities (excluding player vehicles).", function()
        if InSession() then
            local vehicleCount = cleanseEntities(entities.get_all_vehicles_as_handles, function(ent) return IS_PED_A_PLAYER(GET_PED_IN_VEHICLE_SEAT(ent, -1)) end)
            local pedCount = cleanseEntities(entities.get_all_peds_as_handles, IS_PED_A_PLAYER)
            local objectCount = cleanseEntities(entities.get_all_objects_as_handles, function() return false end)
            local totalCount = vehicleCount + pedCount + objectCount
            local msg = ""
            if DebugExtra then msg = $"\n(Vehicles: {vehicleCount})\n(Peds: {pedCount})\n(Objects: {objectCount})" end
            notification.notify(scriptname, $"Removed {totalCount} entities.{msg}")
        else
            notification.notify(scriptname, "Can't be done in Story Mode")
        end
    end)
    
    Cleanse:click_slider("Clean Up", {"cleanup"}, "This excludes players and the car they are in.\n1 = Peds\n2 = Vehicles\n3 = Objects\n4 = Pickups\n5 = Ropes", 1, 5, 1, 1, function(on_change)
        local message
        if on_change == 1 then
            local pedCount = cleanseEntities(entities.get_all_peds_as_handles, IS_PED_A_PLAYER)
            message = $"Removed {pedCount} Peds."
        elseif on_change == 2 then
            local vehicleCount = cleanseEntities(entities.get_all_vehicles_as_handles, function(ent) return IS_PED_A_PLAYER(GET_PED_IN_VEHICLE_SEAT(ent, -1)) end)
            message = $"Removed {vehicleCount} Vehicles."
        elseif on_change == 3 then
            local objectCount = cleanseEntities(entities.get_all_objects_as_handles, function() return false end)
            message = $"Removed {objectCount} Objects."
        elseif on_change == 4 then
            local pickupCount = cleanseEntities(entities.get_all_pickups_as_handles, function() return false end)
            message = $"Removed {pickupCount} Pickups."
        elseif on_change == 5 then
            RefWrapper("World>Inhabitants>Delete All Ropes")
            message = "Removed Rope Entities"
        end
        notification.notify(scriptname, message)
    end)
end

local function RegisterWorld()
    local World = menu.my_root():list("World", {}, "")
    local WorldItems = {
        {name = "Traffic Manager", setupFunc = ManageTraffic, hidden = false},
        --{name = "Water Manipulation", setupFunc = ManageTraffic, hidden = false},
        {name = "Clear Area", setupFunc = CleanUp, hidden = false},
    }
    Root(World)
    for _, item in ipairs(WorldItems) do
        local success, result = pcall(function()
            local menuList = World:list(item.name, {}, item.desc or "")
            if item.setupFunc then item.setupFunc(menuList) end
            if item.hidden then menu.set_visible(menuList, false) end
        end)
        if !success then
            util.toast("[Ares] Caught an Exception during World setup", TOAST_DEFAULT | TOAST_CONSOLE)
            if DebugModules then util.log($"[Ares] {item.name}: {result}") end
        end
    end
end

RegisterWorld()