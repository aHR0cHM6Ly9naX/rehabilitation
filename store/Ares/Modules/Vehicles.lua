enum Labels begin
    NOVEH = -474174214
end

local function Root(Vehicle)
    local function isValidVehicleModel(hash)
        return
        IS_THIS_MODEL_A_CAR(hash) or IS_THIS_MODEL_A_BIKE(hash) or
        IS_THIS_MODEL_A_BICYCLE(hash) or IS_THIS_MODEL_A_QUADBIKE(hash)
    end

    Vehicle:click_slider("Vehicle top speed", {"topspeed", "enginemult", "ts", "enginespeed"}, "Overrides the top speed of the current car you're in", 0, 2000, 0, 50, function(speed)
        if GET_VEHICLE_PED_IS_IN(players.user_ped(), false) != 0 then
            entities.request_control(GET_VEHICLE_PED_IS_IN(players.user_ped(), false), 200)
            MODIFY_VEHICLE_TOP_SPEED(GET_VEHICLE_PED_IS_IN(players.user_ped(), false), speed)
        end
    end)

    Vehicle:toggle_loop("Clean Vehicle", {"clv"}, "Cleans the current Vehicle.", function()
        SET_VEHICLE_DIRT_LEVEL(GET_VEHICLE_PED_IS_IN(players.user_ped()), 0)
    end)

    Vehicle:action("Set Vehicle Engine Sound", {"customenginesound"}, "", function(on_click)
        if player_cur_car != 0 then
            util.toast("Please input the vehicle spawn name to use the engine sound of, i.e \"ADDER\"")
            menu.show_command_box("customenginesound ")
        end
    end, function(veh)
        if player_cur_car != 0 then
            local hash = util.joaat(veh)
            if !IS_MODEL_VALID(hash) then 
                util.toast("Invalid vehicle spawn name")
                return
            end
            if isValidVehicleModel(hash) then
                FORCE_USE_AUDIO_GAME_OBJECT(player_cur_car, veh)
                util.toast($"Engine sound set to {veh}")
            else
                util.toast("While that model is valid, it isn't a valid model to use!")
            end
        end
    end)

    Vehicle:toggle("Stance", {}, "Activates stance on vehicles that support it.", function(toggle)
        SET_REDUCED_SUSPENSION_FORCE(player_cur_car, toggle)
    end)

    CarRapidFire = Vehicle:toggle_loop("Rapid Fire", {"rapidfireveh"}, "True rapid fire. No delay, works via spamming fix vehicle", function()
        if !IS_PED_IN_ANY_VEHICLE(players.user_ped(), false) then
            notification.notify(scriptname, lang.get_localised(NOVEH))
            CarRapidFire.value = false
            return
        end
        RefWrapper("Vehicle>Fix Vehicle")
        yield()
    end)
end

local function Wheelie(Wheels)
    local wheelie_val = 0.3
    Wheels:slider_float("Wheelie Power", {"wheeliepower"}, "Sets the power of the wheelie you're doing", 10, 100, 30, 5, function(value)
        wheelie_val = value / 100
    end)
    
    local vehicleWheelie
    vehicleWheelie = Wheels:toggle_loop("Wheelie Launch", {}, "Press Ctrl and W to wheelie.", function()
        local CAutomobile = entities.get_user_vehicle_as_pointer()
        if CAutomobile == 0 then return end
        local vehicleModel = GET_ENTITY_MODEL(entities.get_user_vehicle_as_handle())
        if !IS_THIS_MODEL_A_CAR(vehicleModel) and !IS_THIS_MODEL_AN_AMPHIBIOUS_CAR(vehicleModel) then 
            return 
        end
        local CHandlingData = entities.vehicle_get_handling(CAutomobile)
        memory.write_float(CHandlingData + 0x00EC, IS_CONTROL_PRESSED(0, 71) and IS_CONTROL_PRESSED(0, 280) ? -wheelie_val : 0.5)
    end)
end

local function Nitrous(Nitro)
    nitro_duration = 5000
    nitro_power = 2000
    RequestPTFXAsset = function(asset)
        REQUEST_NAMED_PTFX_ASSET(asset)
        local startTime = os.time()
        while !HAS_NAMED_PTFX_ASSET_LOADED(asset) and os.time() - startTime < 10 do
            yield()
        end
    end

    Nitro:toggle_loop("Nitro", {"nitro"}, "Enables Nitro boost in any vehicle. Use your boost key as normal with this enabled.", function(toggle)
        local ped, car = players.user_ped(), player_cur_car
        if IS_PED_IN_ANY_VEHICLE(ped, true) and car and IS_CONTROL_JUST_PRESSED(357, 357) then
            RequestPTFXAsset("veh_xs_vehicle_mods")
            SET_OVERRIDE_NITROUS_LEVEL(car, true, 100, nitro_power, 99999999999, false)
            SET_ENTITY_MAX_SPEED(car, 2000)
            yield(nitro_duration)
            SET_OVERRIDE_NITROUS_LEVEL(car, false, 0, 0, 0, false)
        end
    end)
    
    Nitro:slider("Nitro duration", {"nitroduration"}, "In seconds", 1, 30, 5, 1, function(val) nitro_duration = val * 1000 end)
    Nitro:slider("Nitro power", {"nitropower"}, "", 1, 10000, 2000, 1, function(val) nitro_power = val end)
end

local function TestVehicles(TestVeh)
    local function IntGlobal(hash, value)
        memory.write_int(memory.script_global(262145 + memory.tunable_offset(hash)), value) 
    end

    local function setTestVehicle(globalVarName, pVehicleList)
        local vehicleClass = {}
        local ignore_duplicates = {} 
        for _, vehicle in ipairs(util.get_vehicles()) do
            local name = util.get_label_text(vehicle.name)
            local hash = util.joaat(vehicle.name)
            local vehClassName = GET_VEHICLE_CLASS_FROM_NAME(hash)
            if name == "NULL" or ignore_duplicates[name] or (!IS_THIS_MODEL_A_CAR(hash) and !IS_THIS_MODEL_A_BIKE(hash) and !IS_THIS_MODEL_A_QUADBIKE(hash)) then 
                goto continue 
            end
            if !vehicleClass[vehClassName] then 
                vehicleClass[vehClassName] = pVehicleList:list(util.get_label_text($"VEH_CLASS_{vehClassName}"), {}, "")
            end
            ignore_duplicates[name] = true
            vehicleClass[vehClassName]:action(name, {}, "", function()
                IntGlobal(globalVarName, hash)
            end)
            ::continue::
        end
    end

    local pVehicle1 = TestVeh:list("Set Test Vehicle 1", {}, "")
    setTestVehicle("PROMO_TEST_DRIVE_VEHICLE_1_MODEL_HASH", pVehicle1)
    local pVehicle2 = TestVeh:list("Set Test Vehicle 2", {}, "")
    setTestVehicle("PROMO_TEST_DRIVE_VEHICLE_2_MODEL_HASH", pVehicle2)
    local pVehicle3 = TestVeh:list("Set LSCM Test Vehicle 3", {}, "")
    setTestVehicle("PROMO_TEST_DRIVE_VEHICLE_3_MODEL_HASH", pVehicle3)
end

local function ExclusiveVeh(PV)
    local exclusiveVehicle = nil
    local exclusiveVehBlip = nil

    local function setExclusiveVehicleBlip(vehicle)
        if vehicle then
            if exclusiveVehBlip then
                util.remove_blip(exclusiveVehBlip)
            end
            exclusiveVehBlip = ADD_BLIP_FOR_ENTITY(vehicle)
            SET_BLIP_SPRITE(exclusiveVehBlip, exclusiveVehIcon)
            SET_BLIP_COLOUR(exclusiveVehBlip, exclusiveVehColour)
            table.insert(Blips, exclusiveVehBlip)
        end
    end
    
    local function toggleExclusiveVehicle(on)
        local localPed = players.user_ped()
        local vehicle = GET_VEHICLE_PED_IS_IN(localPed, false)
        if vehicle == 0 then
            notification.notify(scriptname, lang.get_localised(NOVEH))
            return
        end
        SET_VEHICLE_EXCLUSIVE_DRIVER(vehicle, localPed, on and 1 or 0)
        local action = on and "Set" or "Removed"
        exclusiveVehicle = on and vehicle or nil
        if on then
            setExclusiveVehicleBlip(vehicle)
        else
            if exclusiveVehBlip then
                util.remove_blip(exclusiveVehBlip)
                exclusiveVehBlip = nil
            end
        end
        notification.notify(scriptname, $"Successfully {action} Current Vehicle as Exclusive Vehicle.")
    end
    
    local function toggleLockVehicle(on)
        if exclusiveVehicle then
            SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(exclusiveVehicle, on)
            local action = on and "Locked" or "Unlocked"
            notification.notify(scriptname, $"{action} your Exclusive Vehicle.")
        else
            notification.notify(scriptname, "No Exclusive Vehicle set.")
        end
    end
    
    local function deleteExclusiveVehicle()
        if exclusiveVehicle then
            if exclusiveVehBlip then 
                util.remove_blip(exclusiveVehBlip)
            end
            entities.delete_by_handle(exclusiveVehicle)
            exclusiveVehicle, exclusiveVehBlip = nil, nil
            notification.notify(scriptname, "Exclusive Vehicle Deleted.")
        else
            notification.notify(scriptname, "No Exclusive Vehicle Currently set!")
        end
    end

    PV:toggle("Set Exclusive Vehicle", {"vehicleexclusive", "mycar"}, "Sets you as the Exclusive Driver of your Current Vehicle.", toggleExclusiveVehicle)
    PV:toggle("Lock Vehicle", {"lockexclusive", "lockmycar"}, "Locks your Exclusive Vehicle.", toggleLockVehicle)
    PV:action("Delete Exclusive Vehicle", {"delexclusive", "delmycar", "deletemycar"}, "Deletes your Current Exclusive Vehicle.", deleteExclusiveVehicle)
    
    exclusiveVehColour = 46
    PV:slider("Exclusive Vehicle Colour", {"ExclusiveVehiclecol"}, "What Colour Exclusive Vehicle should be on the Map.", 1, 86, 5, 1, function(value)
        exclusiveVehColour = value
        if exclusiveVehicle then
            setExclusiveVehicleBlip(exclusiveVehicle)
        end
    end)
    
    exclusiveVehIcon = 782
    PV:slider("Exclusive Vehicle Icon", {"ExclusiveVehicleicon"}, "What Icon Exclusive Vehicle should use on the Map.", 1, 900, 548, 1, function(value)
        exclusiveVehIcon = value
        if exclusiveVehicle then
            setExclusiveVehicleBlip(exclusiveVehicle)
        end
    end)
end

local function RegisterVehicles()
    local Vehicle = menu.my_root():list("Vehicle", {}, "")
    local VehicleItems = {
        {name = "Wheelie", setupFunc = Wheelie, hidden = false},
        {name = "Nitrous", setupFunc = Nitrous, hidden = false},
        {name = "LSCM Vehicles", setupFunc = TestVehicles, hidden = false},
        {name = "Exclusive Vehicle", setupFunc = ExclusiveVeh, hidden = false},
    }
    Root(Vehicle)
    for _, item in ipairs(VehicleItems) do
        local success, result = pcall(function()
            local menuList = Vehicle:list(item.name, {}, item.desc or "")
            if item.setupFunc then item.setupFunc(menuList) end
            if item.hidden then menu.set_visible(menuList, false) end
        end)
        if !success then
            util.toast("[Ares] Caught an Exception during Vehicle setup", TOAST_DEFAULT | TOAST_CONSOLE)
            if DebugModules then util.log($"[Ares] {item.name}: {result}") end
        end
    end
end

RegisterVehicles()