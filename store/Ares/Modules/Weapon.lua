-- @pluto_warnings: disable-var-shadow
local function Root(Weapon)
    local function reloadWhenRolling()
        if GET_IS_TASK_ACTIVE(players.user_ped(), 4) and IS_CONTROL_PRESSED(22, 22) and !IS_PED_SHOOTING(players.user_ped()) then
            yield(900, "ms")
            REFILL_AMMO_INSTANTLY(players.user_ped())
        end
    end
    local function setMaxLockonRange() 
        SET_PLAYER_LOCKON_RANGE_OVERRIDE(players.user(), 99999999.0) 
    end
    local function bypassAntiLockon()
        for _, pid in ipairs(players.list(false, false, true)) do
            local ped = GET_PLAYER_PED_SCRIPT_INDEX(pid)
            if IS_PED_IN_ANY_VEHICLE(ped, false) then
                local vehicle = GET_VEHICLE_PED_IS_USING(ped)
                SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(vehicle, true)
            end
        end
    end
    local function disableBlockBlaming()
        local shouldDisable = IS_CONTROL_PRESSED(24, 24) or IS_CONTROL_PRESSED(25, 25)
        menu.trigger_command(menu.ref_by_path("Online>Protections>Block Blaming"), shouldDisable and "Off" or "On")
    end

	Weapon:toggle_loop("Reload when rolling", {""}, "Reloads your weapon when doing a roll.", reloadWhenRolling)
    Weapon:toggle_loop("Max Lockon Range", {""}, "Sets your player's lockon range with homing missiles and auto aim to the max.", setMaxLockonRange)
    Weapon:toggle_loop("Bypass Anti-Lockon", {""}, "Allows homing missiles to lock onto vehicles with anti-lockon enabled.", bypassAntiLockon)
    Weapon:toggle_loop("Disable Block Blaming When Shooting", {""}, "Disables Block Blaming when you're aiming or shooting", disableBlockBlaming)
end

local function RapidFire(Rapid)
    local function switchWeapon(ped, newWeapon, delay, currentWeapon)
        SET_CURRENT_PED_WEAPON(ped, newWeapon, true)
        yield(delay)
        SET_CURRENT_PED_WEAPON(ped, currentWeapon, true)
    end
	local DelaySystem = Rapid:slider("Delay", {"lrfdelay"}, "This is the delay for all rapid fire options", 1, 1000, 400, 50, function (value); end)
    Rapid:toggle("Legit Rapid Fire", {""}, "Switches to a sticky bomb and back to your Main", function(toggled)
    local ped = players.user_ped()
    if toggled then
        LegitRapidFire = true
        util.create_thread(function()
            while LegitRapidFire do
                if !IS_ENTITY_DEAD(ped) and IS_PED_SHOOTING(ped) then
                    local currentWpMem = memory.alloc()
                    GET_CURRENT_PED_WEAPON(ped, currentWpMem, true)
                    local currentWP = memory.read_int(currentWpMem)
                    switchWeapon(ped, util.joaat("weapon_stickybomb"), menu.get_value(DelaySystem), currentWP)
                end
                yield()
            end
            util.stop_thread()
        end)
    else
        LegitRapidFire = false
    end
    end)
    Rapid:toggle("Sniper Rapid Fire", {""}, "Switches to a sticky bomb and back to your Heavy Sniper MK2", function(toggled)
    local ped = players.user_ped()
    if toggled then
        LegitSRapidFire = true
        util.create_thread(function()
            while LegitSRapidFire do
                if !IS_ENTITY_DEAD(ped) then
                    local currentWeapon = GET_SELECTED_PED_WEAPON(ped)
                    if IS_PED_SHOOTING(ped) and (currentWeapon == util.joaat("weapon_heavysniper_mk2") or currentWeapon == util.joaat("weapon_heavysniper")) then
                        switchWeapon(ped, util.joaat("weapon_stickybomb"), menu.get_value(DelaySystem), currentWeapon)
                    end
                end
                yield()
            end
            util.stop_thread()
        end)
    else
        LegitSRapidFire = false
    end
    end)
    Rapid:toggle("RPG Rapid Fire", {""}, "Switches to a sticky bomb and back to your RPG or Homing Launcher", function(toggled)
    local ped = players.user_ped()
    if toggled then
        LegitRPGRapidFire = true
        util.create_thread(function()
            while LegitRPGRapidFire do
                if !IS_ENTITY_DEAD(ped) then
                    local currentWeapon = GET_SELECTED_PED_WEAPON(ped)
                    if IS_PED_SHOOTING(ped) and (currentWeapon == util.joaat("weapon_hominglauncher") or currentWeapon == util.joaat("weapon_rpg")) then
                        switchWeapon(ped, util.joaat("weapon_stickybomb"), menu.get_value(DelaySystem), currentWeapon)
                    end
                end
                yield()
            end
            util.stop_thread()
        end)
    else
        LegitRPGRapidFire = false
    end
    end)
end

local function OrbGun(Orb)
    local function setBit(addr: number, bit: number) memory.write_int(addr, memory.read_int(addr) | 1 << bit) end
    local function clearBit(addr: number, bit: number) memory.write_int(addr, memory.read_int(addr) ~ 1 << bit) end
    local function create_orbital_cannon_explosion(Position)
        local player_ped = players.user_ped()
        if orbitalStrikeSettings.IsOwned then
            setBit(memory.script_global(2657971 + 1 + (players.user() * 465) + 426), 0)
            yield(1, "s")
            ADD_OWNED_EXPLOSION(player_ped, Position.x, Position.y, Position.z + 1, 59, 1, true, false, 1.0, false)
        else
            ADD_EXPLOSION(Position.x, Position.y, Position.z + 1, 59, 1, true, false, 1.0, false)
        end
        REQUEST_NAMED_PTFX_ASSET("scr_xm_orbital")
        while !HAS_NAMED_PTFX_ASSET_LOADED("scr_xm_orbital") do yield() end
        USE_PARTICLE_FX_ASSET("scr_xm_orbital")
        START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_xm_orbital_blast", Position.x, Position.y, Position.z + 1, 0, 180, 0, 1.0, true, true, true)
        if !orbitalStrikeSettings.MuteOrb then
            for i = 1, 4 do 
                PLAY_SOUND_FROM_ENTITY(-1, "DLC_XM_Explosions_Orbital_Cannon", player_ped, 0, true, false) 
            end
        end
        if orbitalStrikeSettings.IsOwned then
            yield(1, "s")
            clearBit(memory.script_global(2657971 + 1 + (players.user() * 465) + 426), 0)
        end
    end
    local function orbitalStrikeGun()
        local last_hit_coords = v3.new()
        if GET_PED_LAST_WEAPON_IMPACT_COORD(players.user_ped(), last_hit_coords) then
            create_orbital_cannon_explosion(last_hit_coords)
        end
    end
	local orbitalStrikeFeatures = {
        {
            name = "Mute Sound",
            desc = "Mutes the sound from the Orbital Strike",
            type = "toggle",
            action = function(toggled) orbitalStrikeSettings.MuteOrb = toggled end
        },
        {
            name = "Owned Kill",
            desc = "There is a delay with this due to setting a global",
            type = "toggle",
            action = function(toggled) orbitalStrikeSettings.IsOwned = toggled end
        },
        {
            name = "Orbital Strike Gun",
            desc = "",
            type = "toggle_loop",
            action = orbitalStrikeGun
        }
    }

    local function setupOrbitalStrikeFeatures(Orbs, features)
        for _, feature in ipairs(features) do
            if feature.type == "toggle" then
                Orb:toggle(feature.name, {""}, feature.desc, feature.action)
            elseif feature.type == "toggle_loop" then
                Orb:toggle_loop(feature.name, {""}, feature.desc, feature.action)
            end
        end
    end

	orbitalStrikeSettings = {MuteOrb = false,IsOwned = false}
    setupOrbitalStrikeFeatures(Orb, orbitalStrikeFeatures)
end

local function BlipShit(Blips)
    local blipToggles = {
        projectiles = false, missiles = false,
        bombs = false, grenades = false,
        mines = false, misc = false
    }
    local objectUses = 0
    local function modifyUseCount(type, increment)
        if type == "object" then
            if objectUses <= 0 and increment < 0 then
                return
            end
            objectUses = objectUses + increment
        end
    end

    local projectileTypes = {
        missiles = {
            hashes = {
                "w_ex_vehiclemissile_1", "w_ex_vehiclemissile_2", "w_ex_vehiclemissile_3", "w_ex_vehiclemissile_4",
                "w_lr_rpg_rocket", "w_lr_homing_rocket", "w_lr_firework_rocket", "xm_prop_x17_silo_rocket_01",
                "w_arena_airmissile_01a", "w_smug_airmissile_01b", "w_smug_airmissile_02", "w_battle_airmissile_01",
                "h4_prop_h4_airmissile_01a"
            },
            color = 75,
            icon = 548
        },
        bombs = {
            hashes = {
                "w_smug_bomb_01", "w_smug_bomb_02", "w_smug_bomb_03", "w_smug_bomb_04"
            },
            color = 69,
            icon = 368
        },
        grenades = {
            hashes = {
                "w_ex_vehiclemortar", "w_ex_grenadefrag", "w_ex_grenadesmoke", "w_ex_molotov",
                "w_ex_pipebomb", "w_ex_snowball", "w_ex_vehiclegrenade", "w_lr_40mm"
            },
            color = 47,
            icon = 486
        },
        mines = {
            hashes = {
                "w_ex_apmine", "w_ex_arena_landmine_01b", "w_ex_pe", "w_ex_vehiclemine",
                "xm_prop_x17_mine_01a", "xm_prop_x17_mine_02a", "gr_prop_gr_pmine_01a"
            },
            color = 77,
            icon = 653
        },
        misc = {
            hashes = {
                "w_ex_birdshat", "w_ex_snowball", "w_pi_flaregun_shell", "w_am_flare", 
                "w_lr_ml_40mm", "w_sr_heavysnipermk2_mag_ap2"
            },
            color = 46,
            icon = 443
        }
    }

    local function isProjectileModel(model, projectileType)
        for _, hash in ipairs(projectileType.hashes) do
            if util.joaat(hash) == model then
                return true
            end
        end
        return false
    end

    local blipToggleMenu = Blips:list("Toggle", {""}, "Toggle Options for Projectile Blips")
    local blipColorMenu = Blips:list("Colour", {""}, "Colour Options for Projectile Blips")
    local blipIconMenu = Blips:list("Icons", {""}, "Icons Options for Projectile Blips")

    local order = table.freeze{
        "missiles",
        "bombs",
        "grenades",
        "mines",
        "misc"
    }
    blipToggleMenu:toggle("Enable", {}, "Enables marking of the selected things below", function(isEnabled)
        blipToggles.projectiles = isEnabled
    end)
    for _, name in ipairs(order) do
        blipToggleMenu:toggle("Mark " .. name:gsub("^%l", string.upper), {"mark" .. name}, "Marks " .. name .. " on the map.", function(isEnabled)
            blipToggles[name] = isEnabled
            modifyUseCount("object", isEnabled and 1 or -1)
        end)
    end
    for _, name in ipairs(order) do
        local projType = projectileTypes[name]
        blipColorMenu:slider(name:gsub("^%l", string.upper) .. " Colour", {"mark" .. name .. "col"}, "Set the color of " .. name .. " markers.", 1, 85, projType.color, 1, function(value)
            projType.color = value
        end)
        blipIconMenu:slider(name:gsub("^%l", string.upper) .. " Icon", {"mark" .. name .. "icon"}, "Set the icon of " .. name .. " markers.", 1, 900, projType.icon, 1, function(value)
            projType.icon = value
        end)
    end

    util.create_thread(function()
        local projectileBlips = {}
        while true do
            for key, blip in pairs(projectileBlips) do
                if !DOES_ENTITY_EXIST(GET_BLIP_INFO_ID_ENTITY_INDEX(blip)) then
                    util.remove_blip(blip)
                    projectileBlips[key] = nil
                end
            end
            if objectUses > 0 and blipToggles.projectiles then
                local allObjects = entities.get_all_objects_as_handles()
                for _, obj in pairs(allObjects) do
                    local model = GET_ENTITY_MODEL(obj)
                    for _, projType in ipairs(order) do
                        local typeData = projectileTypes[projType]
                        if isProjectileModel(model, typeData) and GET_BLIP_FROM_ENTITY(obj) == 0 then
                            local projBlip = ADD_BLIP_FOR_ENTITY(obj)
                            SET_BLIP_SPRITE(projBlip, typeData.icon)
                            SET_BLIP_COLOUR(projBlip, typeData.color)
                            table.insert(projectileBlips, projBlip)
                            break
                        end
                    end
                end
            end
            util.yield(1000)
        end
    end)
end

local function RegisterWeapons()
    local Weapon = menu.my_root():list("Weapon", {}, "")
    local WeaponItems = {
        {name = "Rapid Fire", setupFunc = RapidFire, hidden = false},
        {name = "Orbital Strike Gun", setupFunc = OrbGun, hidden = false},
        {name = "Blips", setupFunc = BlipShit, hidden = false}
    }
    Root(Weapon)
    for _, item in ipairs(WeaponItems) do
        local success, result = pcall(function()
            local menuList = Weapon:list(item.name, {}, item.desc or "")
            if item.setupFunc then item.setupFunc(menuList) end
            if item.hidden then menu.set_visible(menuList, false) end
        end)
        if !success then
            util.toast("[Ares] Caught an Exception during Weapons setup", TOAST_DEFAULT | TOAST_CONSOLE)
            if DebugModules then util.log($"[Ares] {item.name}: {result}") end
        end
    end
end

RegisterWeapons()