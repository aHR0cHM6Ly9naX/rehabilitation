local function Root(Online)
    Online:toggle_loop("Voice Chat Monitor", {}, "Notifies you of players using voice chat.", function()
        for _, pid in pairs(players.list()) do if NETWORK_IS_PLAYER_TALKING(pid) then 
                notification.notify(scriptname, $"{players.get_name(pid)} is using voice-chat")
            end 
        end
    end)
end

local function PlayerLogger(Logger)
    local function loadCSV(filePath)
        local data = {}
        local file = io.open(filePath, "r")
        if not file then
            util.toast("[Ares] Error: Unable to open file", TOAST_DEFAULT | TOAST_LOGGER)
            return nil
        end
        for line in file:lines() do
            local row = {}
            local timestamp, user, rid, hex, ip = line:match("Seen: ([^|]+) | User: ([^|]+) | Rid: ([^|]+) | Hex: ([^|]+) | IP: ([^|]+)")
            if user and rid and ip then
                table.insert(row, user:match("^%s*(.-)%s*$"))
                table.insert(row, rid:match("^%s*(.-)%s*$"))
                table.insert(row, ip:match("^%s*(.-)%s*$"))
                table.insert(data, row)
            end
        end
        file:close()
        return data
    end

    local function lookup(data, columnIndex, value)
        local results = {}
        local resultSet = {}
        local key, user, rid, ip

        for _, row in ipairs(data) do
            if row[columnIndex] and row[columnIndex] == value then
                user = row[1]
                rid = row[2]
                ip = row[3]
                if not resultSet[ip] then
                    resultSet[ip] = true
                    table.insert(results, {user = user, rid = rid, ip = ip})
                end
            end
        end
        return #results > 0 and results or nil
    end

    local function lookupByName(name)
        local data = loadCSV(filesystem.store_dir() .. "Ares\\Players.csv")
        if not data then return nil end
        return lookup(data, 1, name)
    end

    local function lookupByIP(ip)
        if ip == "Connected via Relay" then
            util.toast("[Ares] \"Connected Via Relay\" isn't Supported", TOAST_DEFAULT | TOAST_LOGGER)
            return nil
        end
        local data = loadCSV(filesystem.store_dir() .. "Ares\\Players.csv")
        if not data then return nil end
        return lookup(data, 3, ip:gsub(" %(VPN%)$", ""))
    end

    local function lookupByRID(rid)
        local data = loadCSV(filesystem.store_dir() .. "Ares\\Players.csv")
        if not data then return nil end
        return lookup(data, 2, rid)
    end

    local function performLookup(lookupFunction, inputType, input)
        local results = lookupFunction(input)
        if results then
            for _, result in ipairs(results) do
                util.toast($"[Ares] User: {result.user} | RID: {result.rid} | IP: {result.ip}", TOAST_DEFAULT | TOAST_CONSOLE)
            end
        else
            util.toast($"[Ares] No result found for {inputType}: {input}", TOAST_DEFAULT | TOAST_CONSOLE)
        end
    end

    local function createLookupAction(label, command, hint, lookupFunction, inputType)
        Logger:action(label, {command}, hint, function()
            menu.show_command_box(command .. " ")
        end, function(input)
            performLookup(lookupFunction, inputType, input)
        end)
    end

    Logger:toggle("Enable Player Logger", {}, "Logs players with details (Seen, User, Rid, Hexed Rid, and IP/VPN Check)", function(on)
        enablePlayerLogger = on
    end)

    createLookupAction("Lookup by Name", "lookupname", "Enter the *EXACT* player name to look up.", lookupByName, "name")
    createLookupAction("Lookup by IP", "lookupip", "Enter the *EXACT* IP address to look up. Will not work with 'Connected Via Relay'", lookupByIP, "IP")
    createLookupAction("Lookup by RID", "lookuprid", "Enter the *EXACT* Rockstar ID (RID) to look up.", lookupByRID, "RID")
end

local function Banking(bank)
    local function handleBanking(actionType)
        if actionType == "withdraw" then
            local bankBalance = NETWORK_GET_VC_BANK_BALANCE(til.get_char_slot())
            NET_GAMESERVER_TRANSFER_BANK_TO_WALLET(til.get_char_slot(), bankBalance)
        elseif actionType == "deposit" then
            local walletBalance = NETWORK_GET_VC_WALLET_BALANCE(til.get_char_slot())
            if walletBalance == 500000 then
                NET_GAMESERVER_TRANSFER_WALLET_TO_BANK(til.get_char_slot(), tostring(walletBalance))
            end
        end
    end
    local function addBankingOption(name, description, loop, actionType)
        if loop then
            bank:toggle_loop(name, {}, description, function() handleBanking(actionType) end)
        else
            bank:action(name, {}, description, function() handleBanking(actionType) end)
        end
    end
    local bankingOptions = {
        {name = "Withdraw", desc = "Withdraw as much money as possible", loop = false, action = "withdraw"},
        {name = "Auto Withdraw", desc = "Automatically withdraw as much money as possible", loop = true, action = "withdraw"},
        {name = "Deposit", desc = "Deposits 500k", loop = false, action = "deposit"},
        {name = "Auto Deposit", desc = "Automatically deposit when the amount hits 500k", loop = true, action = "deposit"}
    }
    for _, option in ipairs(bankingOptions) do addBankingOption(option.name, option.desc, option.loop, option.action) end
end

local function PresetCRC(pcrc)
    local function ExtraChecksumToggle()
        local ECT = menu.ref_by_path("Online>Transitions>Matchmaking>Asset Hashes>Override Extra Checksum")
        if ECT:getState() ~= "Off" then
            ECT:setState("On")
        end
    end
    pcrc:action("Random Matchmaking Code", {"rcrc"}, "Gives you a random crc\nWill give you a random 8 digit code for your asset checksum & your extra checksum\n\nPrinted your CRC to console, in form of a command chain to make it easier to share", function()
        local random2 = math.random(10000000, 99999999)
        local random = math.random(10000000, 99999999)
        ExtraChecksumToggle()
        menu.trigger_commands($"assetchecksum {random}")
        menu.trigger_commands($"extravalue {random2}")
        util.toast($"assetchecksum {random}; extravalue {random2}", TOAST_CONSOLE)
    end)
    
    pcrc:action("CRC Matchmaking", {"crcmm"}, "Gives you a hardcoded crc", function()
        local hardcrc = 90914811
        local hardecrc =  75535695
        ExtraChecksumToggle()
        menu.trigger_commands($"assetchecksum {hardcrc}")
        menu.trigger_commands($"extravalue {hardecrc}")
    end)

    pcrc:action("Ares Matchmaking", {}, "Gives you a matchmaking crc based on Ares version", function()
        local AresMM = "F5418048"
        ExtraChecksumToggle()
        menu.trigger_commands($"assetchecksum {AresMM}")
        menu.trigger_commands($"extravalue {AresMM}")
        util.toast($"[{scriptname}] Your checksum is {AresMM} & your extrachecksum is {AresMM}", TOAST_DEFAULT | TOAST_CONSOLE)
    end)
end

local function CustomCRC(ccrc)
    local checksum = 10000000
    local extrachecksum = 10000000

    local function ExtraChecksumToggle()
        local ECT = menu.ref_by_path("Online>Transitions>Matchmaking>Asset Hashes>Override Extra Checksum")
        if ECT:getState() ~= "Off" then
            ECT:setState("On")
        end
    end

    ccrc:slider("Checksum", {"cChecksum"}, "Allows you to set your own crc", 10000000, 99999999 , 10000000, 1, function(crc)
        checksum = crc
    end)
    
    ccrc:slider("Extra Checksum", {"eChecksum"}, "Allows you to set your own crc", 10000000, 99999999 , 10000000, 1, function(ecrc)
        extrachecksum = ecrc
    end)
    
    ccrc:action("Custom matchmaking", {"ccrc"}, "", function()
        local crc = checksum
        local ecrc = extrachecksum
        ExtraChecksumToggle()
        menu.trigger_commands($"assetchecksum {crc}")
        menu.trigger_commands($"extravalue {ecrc}")
    end)
end

local function RegisterOnline()
    local Online = menu.my_root():list("Online", {}, "")
    local parents = {}
    local OnlineItems = {
        {name = "Player Logger", setupFunc = PlayerLogger, hidden = false},
        {name = "Banking", setupFunc = Banking, hidden = false},
        {name = "Matchmaking", setupFunc = MatchMake, hidden = false},
        {name = "Preset CRC", parent = "Matchmaking", setupFunc = PresetCRC, hidden = false},
        {name = "Custom CRC", parent = "Matchmaking", setupFunc = CustomCRC, hidden = false},
    }
    Root(Online)
    for _, item in ipairs(OnlineItems) do
        local success, result = pcall(function()
            local menuList
            if item.parent then
                if !parents[item.parent] then 
                    util.toast($"[Ares] Caught an Exception (\"{item.parent}\")")
                else
                    menuList = parents[item.parent]:list(item.name, {}, item.desc or "")
                end
            else
                menuList = Online:list(item.name, {}, item.desc or "")
            end
            if !item.parent then parents[item.name] = menuList end
            if item.setupFunc then item.setupFunc(menuList) end
            if item.hidden then menu.set_visible(menuList, false) end
        end)
        if !success then
            util.toast("[Ares] Caught an Exception during Online setup", TOAST_DEFAULT | TOAST_CONSOLE)
            if DebugModules then util.log($"[Ares] {item.name}: {result}") end
        end
    end
end

RegisterOnline()