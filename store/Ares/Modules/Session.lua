local function Kicks(Kick)
    Kick:action("Kick", {""}, "Removes everyone that it can", function()
        for i = 0, 31, 1 do
            if players.exists(i) and i ~= players.user() then
                if !Host_Check() then
                    OnlineRefWrapper(menu.player_root(pid), "Kick>Love Letter")
                elseif !players.is_marked_as_modder(pid) then
                    OnlineRefWrapper(menu.player_root(pid), "Kick>Smart")
                    yield(100)
                end
            end
        end
    end)
    Kick:action("Array", {""}, "Must be in a Vehicle!", function()
        if InSession() then
            local playerVehicle = GET_VEHICLE_PED_IS_IN(players.user_ped(), false)
            if playerVehicle == 0 then
                notification.notify(scriptname, "Get in a Vehicle")
            end
            local ints = table.freeze{
                "FMDeliverableID", "Not_Allow_As_Saved_Veh", "MPBitset", "Player_Vehicle",
                "Player_Hacker_Truck", "CreatedByPegasus", "PYV_Owner", "MC_EntityID",
                "MatchId", "MissionType", "RespawnVeh", "Player_Owned_Veh",
                "GBMissionFire", "Veh_Modded_By_Player", "bombdec", "bombowner",
                "Previous_Owner", "ExportVehicle", "ContrabandOwner", "ContrabandDeliveryType",
                "VehicleList"
            }
            for ints as i do
                DECOR_SET_INT(player_cur_car, i, math.random(9999, 2147483647))
            end
        else
            notification.notify(scriptname, "Be completely loaded in!")
        end
    end)
end

local function Parachute(Crash)
    local function LoadModel(hash)
        REQUEST_MODEL(hash)
        while !HAS_MODEL_LOADED(hash) do
            util.yield()
        end
    end
    
    local function Performance(ped, hash)
        SET_PLAYER_PARACHUTE_MODEL_OVERRIDE(PLAYER_ID(), hash)
        SET_ENTITY_COORDS_NO_OFFSET(ped, 0, 0, 500, 0, 0, 1)
        GIVE_DELAYED_WEAPON_TO_PED(ped, 0xFBAB5776, 1000, false)
        util.yield(1000)
        for i = 0, 20 do
            FORCE_PED_TO_OPEN_PARACHUTE(ped)
        end
        util.yield(1000)
    end
    local function FemboysAreHot(hash_1, hash_2)
        local ped = GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID())
        LoadModel(hash_1)
        Performance(ped, hash_1)
        LoadModel(hash_2)
        Performance(ped, hash_2)
    end

    local CrashSetup = table.freeze{
        {
            name = "Thigh High Crash",
            action = function()
                local ped = GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID())
                local previous_pos = GET_ENTITY_COORDS(ped, true)
                for n = 0, 3 do
                    FemboysAreHot(util.joaat("prop_logpile_06b"), util.joaat("prop_beach_parasol_03"))
                    SET_ENTITY_COORDS_NO_OFFSET(ped, previous_pos.x, previous_pos.y, previous_pos.z, false, true, true)
                end
            end
        },
        {
            name = "Meow Crash",
            action = function()
                local ped = GET_PLAYER_PED_SCRIPT_INDEX(PLAYER_ID())
                local previous_pos = GET_ENTITY_COORDS(ped, true)
                for n = 0, 5 do
                    FemboysAreHot(1381105889, 720581693)
                    SET_ENTITY_COORDS_NO_OFFSET(ped, previous_pos.x, previous_pos.y, previous_pos.z, false, true, true)
                end
            end
        }
    }
    
    for _, command in ipairs(CrashSetup) do
        Crash:action(command.name, command.aliases or {""}, command.description or "", command.action)
    end
end

local function ExecuteSession(Lobby)
    local lobbyKillerScripts = table.freeze{
        {name = "Execute Lobby v1", script = "Range_Modern_MP"}, {name = "Execute Lobby v2", script = "FM_Race_Controler"},
        {name = "Execute Lobby v3", script = "fm_Bj_race_controler"}, {name = "Execute Lobby v4", script = "GTA Online Intro"}, 
        {name = "Execute Lobby v5", script = "fm_mission_controller"}, {name = "Execute Lobby v6", script = "FM_Survival_Controller"},
        {name = "Execute Lobby v7", script = "fm_deathmatch_controler"}, {name = "Execute Lobby v8", script = "tennis_network_mp"},
        {name = "Execute Lobby v9", script = "fm_content_ufo_abduction"}, {name = "Execute Lobby v10", script = "golf_mp"}
    }
    for _, script in ipairs(lobbyKillerScripts) do
        Lobby:action(script.name, {""}, $"This uses {script.script}", function()
            if !isEditionValid(3) then return end
            RefWrapper($"Online>Session>Session Scripts>Run Script>Session Breaking>{script.script}")
            notification.notify(scriptname, $"Using {script.script}\nExecuting the lobby...")
        end)
    end
end

local function RegisterSession()
    local AresLobby = menu.attach_before(menu.ref_by_path("Players>All Players>Show Camera"), menu.list(menu.shadow_root(), "Ares Lobby", {}, ""))
    local SessionItems = {
        {name = "Kick", setupFunc = Kicks, hidden = false},
        {name = "Crash", setupFunc = Parachute, hidden = false},
        {name = "Execute Session", setupFunc = ExecuteSession, hidden = false}
    }
    for _, item in ipairs(SessionItems) do
        local success, result = pcall(function()
            local menuList = AresLobby:list(item.name, {}, item.desc or "")
            if item.setupFunc then item.setupFunc(menuList) end
            if item.hidden then menu.set_visible(menuList, false) end
        end)
        if !success then
            util.toast("[Ares] Caught an Exception during Session setup", TOAST_DEFAULT | TOAST_CONSOLE)
            if DebugModules then util.log($"[Ares] {item.name}: {result}") end
        end
    end
end

RegisterSession()